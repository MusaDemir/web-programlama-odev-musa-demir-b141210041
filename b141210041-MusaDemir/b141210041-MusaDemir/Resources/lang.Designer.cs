﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace b141210041_MusaDemir.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class lang {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal lang() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("b141210041_MusaDemir.Resources.lang", typeof(lang).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Açıklama.
        /// </summary>
        public static string aciklama {
            get {
                return ResourceManager.GetString("aciklama", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adet.
        /// </summary>
        public static string adet {
            get {
                return ResourceManager.GetString("adet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Admin İşlemleri.
        /// </summary>
        public static string adminislemleri {
            get {
                return ResourceManager.GetString("adminislemleri", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adreslerim.
        /// </summary>
        public static string adreslerim {
            get {
                return ResourceManager.GetString("adreslerim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adres Seçiniz.
        /// </summary>
        public static string adressecin {
            get {
                return ResourceManager.GetString("adressecin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ad / Soyad.
        /// </summary>
        public static string adsoyad {
            get {
                return ResourceManager.GetString("adsoyad", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Satın Aldığım Ürünler.
        /// </summary>
        public static string aldiklarim {
            get {
                return ResourceManager.GetString("aldiklarim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alışverişe Başla.
        /// </summary>
        public static string alisverisebasla {
            get {
                return ResourceManager.GetString("alisverisebasla", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Anasayfa Kategori Kontrol.
        /// </summary>
        public static string anasayfakategorikontrol {
            get {
                return ResourceManager.GetString("anasayfakategorikontrol", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Anasayfa Slider Kontrol.
        /// </summary>
        public static string anasayfasliderkoanasayfasliderkontrol {
            get {
                return ResourceManager.GetString("anasayfasliderkoanasayfasliderkontrol", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ara.
        /// </summary>
        public static string ara {
            get {
                return ResourceManager.GetString("ara", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ayarla.
        /// </summary>
        public static string ayarla {
            get {
                return ResourceManager.GetString("ayarla", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tamamlandı.
        /// </summary>
        public static string basarili {
            get {
                return ResourceManager.GetString("basarili", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Başlık.
        /// </summary>
        public static string baslik {
            get {
                return ResourceManager.GetString("baslik", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bilgileriniz kayıt altına alındı. Satın alma işlemini gerçekleştirmek istiyormusunuz ?.
        /// </summary>
        public static string bilgilerkaydedildiode {
            get {
                return ResourceManager.GetString("bilgilerkaydedildiode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çıkış Yap.
        /// </summary>
        public static string cikisyap {
            get {
                return ResourceManager.GetString("cikisyap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cinsiyet.
        /// </summary>
        public static string cinsiyet {
            get {
                return ResourceManager.GetString("cinsiyet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çok Satan.
        /// </summary>
        public static string coksatan {
            get {
                return ResourceManager.GetString("coksatan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Detay.
        /// </summary>
        public static string detay {
            get {
                return ResourceManager.GetString("detay", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Doğum Tarihi.
        /// </summary>
        public static string dogumtarihi {
            get {
                return ResourceManager.GetString("dogumtarihi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Düzenle.
        /// </summary>
        public static string duzenle {
            get {
                return ResourceManager.GetString("duzenle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E-posta.
        /// </summary>
        public static string email {
            get {
                return ResourceManager.GetString("email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Email Değiştir.
        /// </summary>
        public static string emaildegistir {
            get {
                return ResourceManager.GetString("emaildegistir", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En Düşük Fiyat.
        /// </summary>
        public static string endusukfiyat {
            get {
                return ResourceManager.GetString("endusukfiyat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En Yeni.
        /// </summary>
        public static string enyeniurun {
            get {
                return ResourceManager.GetString("enyeniurun", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En Yüksek Fiyat.
        /// </summary>
        public static string enyuksekfiyat {
            get {
                return ResourceManager.GetString("enyuksekfiyat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to En Yüksek Puan.
        /// </summary>
        public static string enyuksekpuan {
            get {
                return ResourceManager.GetString("enyuksekpuan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Eski Şifre.
        /// </summary>
        public static string eskisifre {
            get {
                return ResourceManager.GetString("eskisifre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Evet.
        /// </summary>
        public static string evet {
            get {
                return ResourceManager.GetString("evet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fatura Kesilecek Kişinin Bilgileri.
        /// </summary>
        public static string faturabilgileri {
            get {
                return ResourceManager.GetString("faturabilgileri", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Filtrele.
        /// </summary>
        public static string filtrele {
            get {
                return ResourceManager.GetString("filtrele", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Geri.
        /// </summary>
        public static string geri {
            get {
                return ResourceManager.GetString("geri", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giriş Yap.
        /// </summary>
        public static string girisyap {
            get {
                return ResourceManager.GetString("girisyap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tümünü Görüntüle.
        /// </summary>
        public static string goruntule {
            get {
                return ResourceManager.GetString("goruntule", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Güvenlik Sorusu Cevabı.
        /// </summary>
        public static string gsorucevap {
            get {
                return ResourceManager.GetString("gsorucevap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Güvenlik Sorusu.
        /// </summary>
        public static string guvenliksorusu {
            get {
                return ResourceManager.GetString("guvenliksorusu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hayır !.
        /// </summary>
        public static string hayir {
            get {
                return ResourceManager.GetString("hayir", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hayır Bilgilerimi Sil!.
        /// </summary>
        public static string hayirbilgilerisil {
            get {
                return ResourceManager.GetString("hayirbilgilerisil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hesabım.
        /// </summary>
        public static string hesabim {
            get {
                return ResourceManager.GetString("hesabim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hoş Geldiniz.
        /// </summary>
        public static string hosgeldiniz {
            get {
                return ResourceManager.GetString("hosgeldiniz", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İlanlarım.
        /// </summary>
        public static string ilanlar {
            get {
                return ResourceManager.GetString("ilanlar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kargo Adı ve Takip Kodu.
        /// </summary>
        public static string kargotakip {
            get {
                return ResourceManager.GetString("kargotakip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kart Bilgileri.
        /// </summary>
        public static string kartbilgileri {
            get {
                return ResourceManager.GetString("kartbilgileri", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kategori Kontrolü.
        /// </summary>
        public static string kategorikontrolu {
            get {
                return ResourceManager.GetString("kategorikontrolu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kategoriler.
        /// </summary>
        public static string kategoriler {
            get {
                return ResourceManager.GetString("kategoriler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kaydet ve Devam Et.
        /// </summary>
        public static string kaydetvedevamet {
            get {
                return ResourceManager.GetString("kaydetvedevamet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kayıt Ol.
        /// </summary>
        public static string kayitol {
            get {
                return ResourceManager.GetString("kayitol", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Marka Kontrol.
        /// </summary>
        public static string markakontrol {
            get {
                return ResourceManager.GetString("markakontrol", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Marka Seç.
        /// </summary>
        public static string markasec {
            get {
                return ResourceManager.GetString("markasec", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Not Ekleme İstemiyorum.
        /// </summary>
        public static string notekleme {
            get {
                return ResourceManager.GetString("notekleme", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to  (Boş Kalabilir).
        /// </summary>
        public static string nrequired {
            get {
                return ResourceManager.GetString("nrequired", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Onayla.
        /// </summary>
        public static string onayla {
            get {
                return ResourceManager.GetString("onayla", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Öne Çıkan Ürünler.
        /// </summary>
        public static string onecikanurunler {
            get {
                return ResourceManager.GetString("onecikanurunler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Özellikler.
        /// </summary>
        public static string ozellikler {
            get {
                return ResourceManager.GetString("ozellikler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reklam Kontrolu.
        /// </summary>
        public static string reklamkontrolu {
            get {
                return ResourceManager.GetString("reklamkontrolu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Resim Ekle.
        /// </summary>
        public static string resimekle {
            get {
                return ResourceManager.GetString("resimekle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Resimler.
        /// </summary>
        public static string resimler {
            get {
                return ResourceManager.GetString("resimler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Satici :.
        /// </summary>
        public static string satici {
            get {
                return ResourceManager.GetString("satici", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Satici Diğer Ürünler.
        /// </summary>
        public static string saticidigerurunler {
            get {
                return ResourceManager.GetString("saticidigerurunler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Satıcının Yıldızı :.
        /// </summary>
        public static string saticiyildizi {
            get {
                return ResourceManager.GetString("saticiyildizi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Satın Al.
        /// </summary>
        public static string satinal {
            get {
                return ResourceManager.GetString("satinal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Satışta mı? :.
        /// </summary>
        public static string satistami {
            get {
                return ResourceManager.GetString("satistami", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Satışa Çıkardığım Ürünler.
        /// </summary>
        public static string sattiklarim {
            get {
                return ResourceManager.GetString("sattiklarim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sepetim.
        /// </summary>
        public static string sepet {
            get {
                return ResourceManager.GetString("sepet", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sepetiniz Bomboş.
        /// </summary>
        public static string sepetbos {
            get {
                return ResourceManager.GetString("sepetbos", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şifre.
        /// </summary>
        public static string sifre {
            get {
                return ResourceManager.GetString("sifre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şifre Değiştir.
        /// </summary>
        public static string sifredegistir {
            get {
                return ResourceManager.GetString("sifredegistir", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sil.
        /// </summary>
        public static string sil {
            get {
                return ResourceManager.GetString("sil", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Siparis Durumu.
        /// </summary>
        public static string siparisdurumu {
            get {
                return ResourceManager.GetString("siparisdurumu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Siparişlerim.
        /// </summary>
        public static string siparislerim {
            get {
                return ResourceManager.GetString("siparislerim", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Son Gezilen Ürünler.
        /// </summary>
        public static string songezilenler {
            get {
                return ResourceManager.GetString("songezilenler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Stok Sayısı:.
        /// </summary>
        public static string stok {
            get {
                return ResourceManager.GetString("stok", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to TC No..
        /// </summary>
        public static string tcno {
            get {
                return ResourceManager.GetString("tcno", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Toplamda.
        /// </summary>
        public static string toplamda {
            get {
                return ResourceManager.GetString("toplamda", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fiyat.
        /// </summary>
        public static string ucret {
            get {
                return ResourceManager.GetString("ucret", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ücret Aralığı.
        /// </summary>
        public static string ucretaraligi {
            get {
                return ResourceManager.GetString("ucretaraligi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Urun.
        /// </summary>
        public static string urun {
            get {
                return ResourceManager.GetString("urun", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aradığınız Kriterlerde Ürün Bulunamadı !.
        /// </summary>
        public static string urunbulunamadi {
            get {
                return ResourceManager.GetString("urunbulunamadi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ürüne Git.
        /// </summary>
        public static string urunegit {
            get {
                return ResourceManager.GetString("urunegit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ürün Yıldızı :.
        /// </summary>
        public static string urunyildizi {
            get {
                return ResourceManager.GetString("urunyildizi", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Üye Kontrolü.
        /// </summary>
        public static string uyekontrolu {
            get {
                return ResourceManager.GetString("uyekontrolu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Bir Adres Eklemek İstiyorum.
        /// </summary>
        public static string yenibiradressekle {
            get {
                return ResourceManager.GetString("yenibiradressekle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Ekle.
        /// </summary>
        public static string yeniekle {
            get {
                return ResourceManager.GetString("yeniekle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Şifre.
        /// </summary>
        public static string yenisifre {
            get {
                return ResourceManager.GetString("yenisifre", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni Şifre Doğrulama.
        /// </summary>
        public static string yenisifretekrar {
            get {
                return ResourceManager.GetString("yenisifretekrar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Formu Yolla.
        /// </summary>
        public static string yolla {
            get {
                return ResourceManager.GetString("yolla", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yorum Kontrolu.
        /// </summary>
        public static string yorumkontrolu {
            get {
                return ResourceManager.GetString("yorumkontrolu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yorumlar Ve Oylar.
        /// </summary>
        public static string yorumlarveoylar {
            get {
                return ResourceManager.GetString("yorumlarveoylar", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yorum Yaz.
        /// </summary>
        public static string yorumyaz {
            get {
                return ResourceManager.GetString("yorumyaz", resourceCulture);
            }
        }
    }
}
