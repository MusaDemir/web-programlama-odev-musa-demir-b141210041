//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace b141210041_MusaDemir.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class markalar
    {
        public markalar()
        {
            this.urunler = new HashSet<urunler>();
        }
    
        public int markaID { get; set; }
        public string markaAd { get; set; }
    
        public virtual ICollection<urunler> urunler { get; set; }
    }
}
