﻿using System;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;


class _MainIndexRoutingAttribute:ActionFilterAttribute//indexi tr/main olarak geri döndüren method
{
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        if (filterContext.HttpContext.Request.Url.ToString()=="http://localhost:52297/")// servera göre değiştirilecek
        {
            filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new
            {
                controller = "main",
                action = "index",
                dil = YardimciMethodlar.dilSessionGetir()

            }));
            return;
        }
        
    }    
}


class _KullaniciCookieKontrolAttribute : ActionFilterAttribute
{// Kullanici cookie varsa giriş yaptı ayarla
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        if (YardimciMethodlar.CookieGetir("cID") != "")
        {
            YardimciMethodlar.KullaniciSessionIDAyarla(Convert.ToInt32(YardimciMethodlar.CookieGetir("cID")));
        }
    }
}
class _KullaniciSessionControlAttribute : ActionFilterAttribute
{// kullanici girişi gerektiren sayfalarda kullanici girişi kontrolu için attribute
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        if (YardimciMethodlar.KullaniciSessionIDGetir() == 0)
        {
            filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new
            {
                controller = "uyeislemleri",
                action = "giris",
                dil = YardimciMethodlar.dilSessionGetir()

            }));
            return;
        }
       
    }
}
class _KullaniciAdminmiKontrolAttribute : ActionFilterAttribute
{// kullanici girişi gerektiren sayfalarda kullanici girişi kontrolu için attribute
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        if (!YardimciMethodlar.uyeAdminmi())
        {
            filterContext.Result = new RedirectToRouteResult(new System.Web.Routing.RouteValueDictionary(new
            {
                controller = "kullanici",
                action = "index",
                dil = YardimciMethodlar.dilSessionGetir()
            }));
            return;
        }

    }
}

class _DilSessionKontrolAttribute : ActionFilterAttribute
{// dil kontrolü
    public override void OnActionExecuting(ActionExecutingContext filterContext)
    {
        if (filterContext.ActionParameters["dil"] != null )
        {
            YardimciMethodlar.dilSessionAyarla(filterContext.ActionParameters["dil"].ToString());
        }
        else
        {
            YardimciMethodlar.dilSessionAyarla("tr");
        }
       
        return;
    }
}