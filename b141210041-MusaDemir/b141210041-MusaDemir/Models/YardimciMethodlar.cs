﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Web;

namespace b141210041_MusaDemir.Models
{
    public class YardimciMethodlar
    {
        private static eticaretEntities db = new eticaretEntities();
        public static bool uyeAdminmi()
        {
            int sID = YardimciMethodlar.KullaniciSessionIDGetir();
            var uyeRol = db.uyeler.Where(x => x.uyeID == sID).Select(x => x.uyeRol).SingleOrDefault();
            if (uyeRol != null && uyeRol == "admin")
            {
                return true;
            }
            return false;
        }

        public static void KullaniciSessionIDAyarla(int id)
        {
            HttpContext.Current.Session["cID"] = id;            
        }
        public static void KullaniciSessionIDSil()
        {
            HttpContext.Current.Session["cID"] = 0;
            if (CookieGetir("cID") != "")
            {
                CookieSil("cID");
            }
        }
        public static int KullaniciSessionIDGetir()
        {
            int id=HttpContext.Current.Session["cID"] == null ? 0 : (int)HttpContext.Current.Session["cID"];
            var uye = db.uyeler.Where(u=>u.uyeID==id).SingleOrDefault();
            if (uye!=null)
            {
                return id;
            }
            return 0;
        }


        public static void CookieEkle(string cookieName, string cookieValue)
        {
            // Cookie Ekleme
            //cookieName : Cookie adı
            //cookieValue : Cookie değeri
            HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
            cookie.Expires = DateTime.Now.AddMonths(1); // Süre(1 ay)
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
        public static void CookieSil(string userCookieName)
        {
            //Cookie Silme
            HttpCookie myCookie = new HttpCookie(userCookieName);
            myCookie.Expires = DateTime.Now.AddYears(-1);// Süreyi  1 sene azaltıyoruz
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }
        public static string CookieGetir(string cookieName)
        {
            // Cookie adıyla değeri çekme

            return HttpContext.Current.Request.Cookies[cookieName] == null ? "" : HttpContext.Current.Request.Cookies[cookieName].Value;
       
        }

        public static void sepeteEkle(int urunid,int sayi)// aynı zamanda değiştirme methodu
        {
            Dictionary<int, int> sepetDictionary;
            if (HttpContext.Current.Session["sepet"] == null)
            {
                sepetDictionary = new Dictionary<int, int>();
                sepetDictionary.Add(urunid, sayi);
            }
            else
            {
                sepetDictionary = HttpContext.Current.Session["sepet"] as Dictionary<int, int>;
                if (sepetDictionary.Keys.Contains(urunid))
                {
                    sepetDictionary[urunid] = sayi;
                }
                else
                {
                    sepetDictionary.Add(urunid, sayi);
                }
            }
             
           
            HttpContext.Current.Session["sepet"] = sepetDictionary;
        }
        public static Dictionary<int, int> sepetGetir()
        {
            return HttpContext.Current.Session["sepet"] == null ? new Dictionary<int, int>() : HttpContext.Current.Session["sepet"] as Dictionary<int, int>;
        }
        public static void sepetiTemizle()
        {
            HttpContext.Current.Session["sepet"] = null;
        }

        public enum kategoriSayfasıUrunSiralamaKriterleri
        {
           enpahali,enucuz,enyeni,coksatan,puanifazla,
        }
        public static void kategoriSayfasıUrunSiralamaKriteriAyarla(kategoriSayfasıUrunSiralamaKriterleri veri)
        {
            HttpContext.Current.Session["kategorisiralama"] = veri;
        }
        public static kategoriSayfasıUrunSiralamaKriterleri kategoriSayfasıUrunSiralamaKriteriGetir()
        {
            return HttpContext.Current.Session["kategorisiralama"] == null ? kategoriSayfasıUrunSiralamaKriterleri.enyeni : (kategoriSayfasıUrunSiralamaKriterleri)HttpContext.Current.Session["kategorisiralama"];
        }

        public static void dilSessionAyarla(string dil)
        {
            if (dil == null || dil.Trim() == String.Empty)
            {
                HttpContext.Current.Session["dil"] = "tr";
                HttpContext.Current.Session["Culture"] = new CultureInfo("tr"); 
            }
            else if (dil == "en")
            {
                HttpContext.Current.Session["dil"] = "en";
                HttpContext.Current.Session["Culture"] = new CultureInfo("en"); 
            }
            else
            {
                HttpContext.Current.Session["dil"] = "tr";
                HttpContext.Current.Session["Culture"] = new CultureInfo("tr"); 
            }
            
        }
        public static string dilSessionGetir()
        {
            if (HttpContext.Current.Session["dil"] == null || HttpContext.Current.Session["dil"].ToString().Trim() == String.Empty)
                  dilSessionAyarla("tr");   
            return HttpContext.Current.Session["dil"].ToString();

        }

        public static string urlTemizle(string gelenurl)
        {
            gelenurl = gelenurl.Replace("ş", "s");
            gelenurl = gelenurl.Replace("Ş", "s");
            gelenurl = gelenurl.Replace("İ", "i");
            gelenurl = gelenurl.Replace("I", "i");
            gelenurl = gelenurl.Replace("ı", "i");
            gelenurl = gelenurl.Replace("ö", "o");
            gelenurl = gelenurl.Replace("Ö", "o");
            gelenurl = gelenurl.Replace("ü", "u");
            gelenurl = gelenurl.Replace("Ü", "u");
            gelenurl = gelenurl.Replace("Ç", "c");
            gelenurl = gelenurl.Replace("ç", "c");
            gelenurl = gelenurl.Replace("ğ", "g");
            gelenurl = gelenurl.Replace("Ğ", "g");
            gelenurl = gelenurl.Replace(" ", "-");
            gelenurl = gelenurl.Replace("---", "-");
            gelenurl = gelenurl.Replace("?", "");
            gelenurl = gelenurl.Replace("/", "");
            gelenurl = gelenurl.Replace(".", "");
            gelenurl = gelenurl.Replace("'", "");
            gelenurl = gelenurl.Replace("#", "");
            gelenurl = gelenurl.Replace("%", "");
            gelenurl = gelenurl.Replace("&", "");
            gelenurl = gelenurl.Replace("*", "");
            gelenurl = gelenurl.Replace("!", "");
            gelenurl = gelenurl.Replace("@", "");
            gelenurl = gelenurl.Replace("+", "");
 
            gelenurl = gelenurl.ToLower();
            gelenurl = gelenurl.Trim();
             
            // tüm harfleri küçült
            string encodedUrl = (gelenurl ?? "").ToLower();
 
            // & ile " " yer değiştirme
            encodedUrl = Regex.Replace(encodedUrl, @"&+", "and");
 
            // " " karakterlerini silme
            encodedUrl = encodedUrl.Replace("'", "");
 
            // geçersiz karakterleri sil
            encodedUrl = Regex.Replace(encodedUrl, @"[^a-z0-9]", "-");
 
            // tekrar edenleri sil
            encodedUrl = Regex.Replace(encodedUrl, @"-+", "-");
 
            // karakterlerin arasına tire koy
            encodedUrl = encodedUrl.Trim('-');

            if (encodedUrl.Length>250)
            {
                encodedUrl = encodedUrl.Substring(0, 250);
            }
            return encodedUrl;
        }

        public static string urldiliDegistir(string url,string eski, string yeni)
        {
            for (int i = 2; i < url.Length - 2; i++)
            {
                string gecici = url[i].ToString() + url[i + 1].ToString() + url[i + 2].ToString(); ;
                
                if (gecici == eski)
                {
                    string a = url.Substring(0, i);
                    string b = url.Substring(i + 3, url.Length - (i + 3));
                    string duzenlenen = a + yeni + b;
                    return duzenlenen;
                }
            }
            return "false";

        }
        public static string sayfaDegistir(string url,int sayfa)
        {
            var list = url.Split('/');
            list[list.Count() - 2] = sayfa.ToString();
            string redirecturl = "";
            for (int i = 0; i < list.Count(); i++)
            {
                if (i == list.Count() - 1)// url sonuna / ekleme
                {
                    redirecturl += list[i];
                }
                else
                {
                    redirecturl += list[i] + "/";
                }
               
            }
          
            if (redirecturl != "")
            {
                return redirecturl;
            }
            return "false";
        }

        public static double urunYildizGetir(int urunid)
        {
            var sayi = db.yorumlar.Where(y => y.urunID == urunid).Average(x => x.yildiz);
            if (sayi != null)
            {
                double donecekveri = Convert.ToDouble(sayi);
                return donecekveri;
            }
            return 0;
        }

        public static void sonGezilenlereEkle(int id)
        {
            List<int> liste = new List<int>();
            if (HttpContext.Current.Session["songezilenler"] != null)
            {
                liste = HttpContext.Current.Session["songezilenler"] as List<int>;
            }
            if (!liste.Contains(id))
            {
                liste.Add(id);
            }
           
            if (liste.Count > 10)
            {
                liste.RemoveAt(0);
            }
            HttpContext.Current.Session["songezilenler"] = liste;
        }
        public static List<urunMiniStruct> sonGezilenUrunleriGetir()
        {
            List<urunMiniStruct> urunliste = new List<urunMiniStruct>();
            List<int> listeurunid = new List<int>();
            if (HttpContext.Current.Session["songezilenler"] != null)
            {
                listeurunid = HttpContext.Current.Session["songezilenler"] as List<int>;
            }
            listeurunid.ForEach(id=>{
                var urun = db.urunler.Where(u => u.urunID == id).SingleOrDefault();
                if (urun != null)
                {
                    urunliste.Add(new urunMiniStruct()
                    {
                        baslik = YardimciMethodlar.dilSessionGetir() == "en" ? urun.basliken : urun.basliktr,
                        guvenliUrl=urun.guvenliurl,
                        resimurl=urun.urunAnaResim,
                        ucret = " &#x20BA;" + urun.yeniFiyat.ToString(),
                        urunID=urun.urunID
                    });
                }

            });
            return urunliste;
        }
        public static List<urunMiniStruct> tavsiyeEdilenler()
        {
            List<urunMiniStruct> tavsiteListe = new List<urunMiniStruct>();
            List<int> listeurunid = new List<int>();
            if (HttpContext.Current.Session["songezilenler"] != null)
            {
                listeurunid = HttpContext.Current.Session["songezilenler"] as List<int>;
            }
            listeurunid.ForEach(id =>
            {
                int katid = db.urunler.Where(u => u.urunID == id).Select(u=>u.kategoriID).SingleOrDefault();
                db.urunler.Where(u => u.kategoriID == katid).OrderByDescending(u => u.satilan).Take(1).ToList().ForEach(u => {
                    urunMiniStruct urunmini = new urunMiniStruct();
                    urunmini.baslik = YardimciMethodlar.dilSessionGetir() == "en" ? u.basliken : u.basliktr;
                    urunmini.guvenliUrl = u.guvenliurl;
                    urunmini.resimurl = u.urunAnaResim;
                    urunmini.ucret = " &#x20BA;" + u.yeniFiyat.ToString();
                    urunmini.urunID = u.urunID;
                    tavsiteListe.Add(urunmini);
                });

            });
            return tavsiteListe;
        }


        #region sifrele
        public static string StringSifrele(string veri)
        {

            if (veri.Trim() == String.Empty || veri.Trim() == "")
            {
                return "hata";
            }
            for (int i = 0; i < 10; i++)
            {
                veri += veri;
            }
            string gecici = veri;
            for (int i = 0; i < 5; i++)
            {
                gecici = MD5(MD5(MD5(gecici)));
                gecici += gecici;
                if (gecici.Length > 1000)
                {
                    gecici = gecici.Substring(0, 999);
                }

            }

            return gecici;
        }

        static string MD5(string strGiris)
        {
            if (strGiris == "" || strGiris == null)
            {
                throw new ArgumentNullException("Şifrelenecek veri yok");
            }
            else
            {
                MD5CryptoServiceProvider sifre = new MD5CryptoServiceProvider();
                byte[] arySifre = ByteDonustur(strGiris);
                byte[] aryHash = sifre.ComputeHash(arySifre);
                return BitConverter.ToString(aryHash);
            }
        }
        static byte[] ByteDonustur(string deger)
        {
            System.Text.UnicodeEncoding ByteConverter = new System.Text.UnicodeEncoding();
            return ByteConverter.GetBytes(deger);
        }
        
        #endregion
       
    }
}