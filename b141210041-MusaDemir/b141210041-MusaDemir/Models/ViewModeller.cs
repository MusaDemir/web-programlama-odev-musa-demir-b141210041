﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace b141210041_MusaDemir.Models
{
    public class notuniqueDictionary<TKey,TValue>
    {
        public TKey KeyVeri { get; set; }
        public TValue ValueVeri { get; set; }
    }
    public class notuniqueDictionary<TKey, TAra, TValue>
    {
        public TKey KeyVeri { get; set; }
        public TValue ValueVeri { get; set; }
        public TAra AraDeger { get; set; }
    }
    public class imagesviewmodel
    {
        public int ID { get; set; }
        public string Url { get; set; }
    }
    public class ModelgirisYap
    {
        [Required(ErrorMessage = "Lütfen mail adresinizi giriniz.")]     
        [StringLength(100, ErrorMessage = "E-mail adresiniz en az 6 en çok 100 karakterden oluşmalıdır.", MinimumLength = 6)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Lütfen şifrenizi giriniz.")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Şifreniz en az 6 en çok 100 karakterden oluşmalıdır.", MinimumLength = 6)]
        public string Sifre { get; set; }

        [Display(Name="Beni Hatırla")]
        public bool BeniHatirla { get; set; }
    }
    public class ModelGeciciKayitOl
    {
        [Required(ErrorMessage = "Ad Soyad Boş Bırakılamaz")]
        [StringLength(100, ErrorMessage = "Ad Soyad en az 6 en çok 100 karakterden oluşmalıdır.", MinimumLength = 6)]
        public string AdSoyad { get; set; }

        [Required(ErrorMessage = "Lütfen mail adresinizi giriniz.")]     
        [StringLength(100, ErrorMessage = "E-mail adresiniz en az 6 en çok 100 karakterden oluşmalıdır.", MinimumLength = 6)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Lütfen şifrenizi giriniz.")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "Şifreniz en az 6 en çok 100 karakterden oluşmalıdır.", MinimumLength = 6)]
        public string Sifre { get; set; }

        public string OnayKodu { get; set; }
    }


    public class Modelkullanici_UrunEkle
    {
        [Required]
        public int kategoriID { get; set; }
        [Required]
        public int markaID { get; set; }
        [Required]
        public int saticiID { get; set; }
        [Required]
        [Display(Name = "Ürün Başlığı")]
        public string basliktr { get; set; }
        [Required]
        [Display(Name ="Ürün Detayı" )]
        public string detaytr { get; set; }
        [Display(Name = "Ürün Başlığı")]
        public string basliken { get; set; }
        [Required]
        [Display(Name = "Ürün Detayı")]
        public string detayen { get; set; }
        [Display(Name = "Ürün Resimler")]
        public IEnumerable<HttpPostedFileBase> resimler { get; set; }
        public IDictionary<string, string> kategoriozellik { get; set; }
        [Required]
        [Display(Name ="Ürün Ücreti" )]
        public double yeniFiyat { get; set; } 
        [Required]
        [Display(Name = "Stok" )]
        public int stok { get; set; }
        public string guvenliurl { get; set; }
    }
    public class Modelkullanici_urunkategoriPartial
    {// urun eklerken, duzenlerken ve kategori düzenlerken kategori özelliklerini getiren partialler için model 
        public int kategoriOzellikleriID { get; set; }
        public string ozellikDetay { get; set; }
        public string ozellikAdiEn { get; set; }
        public string ozellikAdiTR { get; set; }
    }
    public class Modelkullanici_odeme
    {
        public int adresID { get; set; }
        public string AdSoyad { get; set; }
        public string telefonNo { get; set; }
        public string email { get; set; }
        public string aciklama { get; set; }

        public string kartno { get; set; }
        public string sonkullanmatarihi { get; set; }
        public string kartSahibiAdSoyad { get; set; }
    }


    public class ModeladminAnasayfaSlider_listele
    {
        public int sliderID { get; set; }
        public string resimurl { get; set; }
        public string baslik { get; set; }

    }

    public struct urunMiniStruct
    {
        public int urunID { get; set; }
        public string baslik { get; set; }
        public string resimurl { get; set; }
        public string ucret { get; set; }
        public string guvenliUrl { get; set; }
    }
    public class Modelmain_Urun
    {
        public int urunID { get; set; }
        public string baslik { get; set; }
        public int stok { get; set; }
        public int saticiID { get; set; }
        public string satici { get; set; }
        public int urunYildizSayisi { get; set; }
        public int saticiYildizSayisi { get; set; }
        public List<string> resimUrller { get; set; }
        public string fiyat { get; set; }
        public string kategori { get; set; }
        public string urunDetay { get; set; }
        public Dictionary<string, string> ozellikler { get; set; }
        public List<yorumlar> yorumlar { get; set; }
        public List<urunMiniStruct> saticiDigerUrunleri { get; set; }
    }  
    public class Modelmain_Kategori
    {
        public int urunSayisi { get; set; }
        public string kategoriAd { get; set; }
        public double maxUcret { get; set; }
        public double minUcret { get; set; }
        public int sayfaSayisi { get; set; }
        public int cpage { get; set; }
        public List<kategori_kOzellikleristruct> ozellikler { get; set; }
        public List<urunMiniStruct> urunler { get; set; }
    
    }
    public class Modelmain_ara
    {
        public int  urunSayisi { get; set; }
        public int sayfaSayisi { get; set; }
        public int cpage { get; set; }
        public List<urunMiniStruct> urunler { get; set; }
    }
    public class Modelmain_Index
    {
        public List<urunler> sliderUrunler { get; set; }
        public List<urunMiniStruct> sonGezilenler { get; set; }
        public List<notuniqueDictionary<string, string, List<urunMiniStruct>>> tabmenuurunler { get; set; }
        public List<notuniqueDictionary<string,string>> anasayfa6resim { get; set; }

    }


    public struct kategori_kOzellikleristruct
    {
        public int ID { get; set; }
        public string ozellikadi { get; set; }
        public string guvenliurl { get; set; }
        public List<string> ozellikicerik { get; set; }
    }

    public class ModelkullaniciSiparis
    {
        public int siparisID { get; set; }
        public int siparisMiktari { get; set; }
        public double ucret { get; set; }
        public string baslik { get; set; }
        public string resim { get; set; }
        public string kargotakip { get; set; }
        public string urungurl { get; set; }
        public int urunID { get; set; }
        public bool saticiOnay { get; set; }
        public bool aliciOnay { get; set; }
        public string siparisDurum { get; set; }
    }
}