﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    [_KullaniciAdminmiKontrol(Order = 3)]
    [_KullaniciSessionControl(Order = 2)]
    [_KullaniciCookieKontrol(Order = 1)]
    public class adminyorumislemleriController : Controller
    {
        private eticaretEntities db = new eticaretEntities();

        // GET: /adminyorumislemleri/
        public ActionResult Index()
        {
            var yorumlar = db.yorumlar.Include(y => y.urunler);
            return View(yorumlar.ToList());
        }

        // GET: /adminyorumislemleri/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            yorumlar yorumlar = db.yorumlar.Find(id);
            if (yorumlar == null)
            {
                return HttpNotFound();
            }
            return View(yorumlar);
        }

     

        // GET: /adminyorumislemleri/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            yorumlar yorumlar = db.yorumlar.Find(id);
            if (yorumlar == null)
            {
                return HttpNotFound();
            }
            ViewBag.urunID = new SelectList(db.urunler, "urunID", "basliktr", yorumlar.urunID);
            return View(yorumlar);
        }

        // POST: /adminyorumislemleri/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="yorumID,urunID,adSoyad,email,yorum,yildiz,yorumtarihi")] yorumlar yorumlar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(yorumlar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.urunID = new SelectList(db.urunler, "urunID", "basliktr", yorumlar.urunID);
            return View(yorumlar);
        }

        // GET: /adminyorumislemleri/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            yorumlar yorumlar = db.yorumlar.Find(id);
            if (yorumlar == null)
            {
                return HttpNotFound();
            }
            return View(yorumlar);
        }

        // POST: /adminyorumislemleri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            yorumlar yorumlar = db.yorumlar.Find(id);
            db.yorumlar.Remove(yorumlar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
