﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    [_KullaniciAdminmiKontrol(Order = 3)]
    [_KullaniciSessionControl(Order = 2)]
    [_KullaniciCookieKontrol(Order = 1)]

    public class adminanasayfakategoriislemleriController : Controller
    {
        private eticaretEntities db = new eticaretEntities();

        // GET: /adminanasayfakategoriislemleri/
        [_DilSessionKontrol]
        public ActionResult Index(string dil)
        {
            var anasayfakategoriler = db.anasayfakategoriler.Include(a => a.kategoriler);
            return View(anasayfakategoriler.ToList());
        }

        // GET: /adminanasayfakategoriislemleri/Create
        [_DilSessionKontrol]
        public ActionResult Create(string dil)
        {
            var altkategoriliste = new List<kategoriler>();
            db.kategoriler.Where(kn0 => kn0.anakategoriID != 0).ToList().ForEach(kn0 =>
            {

                db.kategoriler.Where(kalt => kalt.kategoriID == kn0.anakategoriID).ToList().ForEach(kalt =>
                {
                    if (kalt.anakategoriID != 0)
                    {
                        altkategoriliste.Add(kn0);
                    }
                });


            });


            string valuestringi = YardimciMethodlar.dilSessionGetir() == "tr" ? "kategoriAdi" : "enkategoriAdi";
            ViewBag.kategorilerID = new SelectList(altkategoriliste, "kategoriID", valuestringi);
            return View();
        }

        // POST: /adminanasayfakategoriislemleri/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,kategorilerID")] anasayfakategoriler anasayfakategoriler)
        {
            if (ModelState.IsValid)
            {
                db.anasayfakategoriler.Add(anasayfakategoriler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var altkategoriliste = new List<kategoriler>();
            db.kategoriler.Where(kn0 => kn0.anakategoriID != 0).ToList().ForEach(kn0 =>
            {

                db.kategoriler.Where(kalt => kalt.kategoriID == kn0.anakategoriID).ToList().ForEach(kalt =>
                {
                    if (kalt.anakategoriID != 0)
                    {
                        altkategoriliste.Add(kn0);
                    }
                });


            });


            string valuestringi = YardimciMethodlar.dilSessionGetir() == "tr" ? "kategoriAdi" : "enkategoriAdi";
            ViewBag.kategorilerID = new SelectList(altkategoriliste, "kategoriID", valuestringi, anasayfakategoriler.kategorilerID);
            return View(anasayfakategoriler);
        }


        // GET: /adminanasayfakategoriislemleri/Delete/5
        [_DilSessionKontrol]
        public ActionResult Delete(int? id, string dil)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            anasayfakategoriler anasayfakategoriler = db.anasayfakategoriler.Find(id);
            if (anasayfakategoriler == null)
            {
                return HttpNotFound();
            }
            return View(anasayfakategoriler);
        }

        // POST: /adminanasayfakategoriislemleri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            anasayfakategoriler anasayfakategoriler = db.anasayfakategoriler.Find(id);
            db.anasayfakategoriler.Remove(anasayfakategoriler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
