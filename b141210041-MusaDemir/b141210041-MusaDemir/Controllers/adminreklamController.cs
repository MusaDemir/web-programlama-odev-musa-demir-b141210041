﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    [_KullaniciAdminmiKontrol(Order = 3)]
    [_KullaniciSessionControl(Order = 2)]
    [_KullaniciCookieKontrol(Order = 1)]
    [_DilSessionKontrol]
    public class adminreklamController : Controller
    {
        private eticaretEntities db = new eticaretEntities();

        // GET: /adminreklam/
        public ActionResult Index(string dil)
        {
            return View(db.reklamlar.ToList());
        }

        // GET: /adminreklam/Edit/5
        public ActionResult Edit(string dil,int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            reklamlar reklamlar = db.reklamlar.Find(id);
            if (reklamlar == null)
            {
                return HttpNotFound();
            }
            int saticiId = YardimciMethodlar.KullaniciSessionIDGetir();
            var veri = db.urunler.Where(x => x.saticiID == saticiId).Select(x => new SelectListItem { Text = x.basliktr, Value = x.guvenliurl }).ToList();
            ViewBag.urunguvenliurl = veri;
            return View(reklamlar);
        }

        // POST: /adminreklam/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,urunguvenliurl,resimurl,aciklama")] reklamlar reklamlar, HttpPostedFileBase resim, string dil)
        {
            if (ModelState.IsValid)
            {
                if (resim != null)
                {
                    string DosyaAdi = Guid.NewGuid().ToString().Replace('-', '1');// sunucumuzda bulunmayan rasgele bir string atıyor aralardaki - leri herhangi bir karakterle değiştiriyoruz
                    string uzanti = System.IO.Path.GetExtension(resim.FileName);
                    string tamYolVeri = "~/images/kullaniciresimleri/" + DosyaAdi + uzanti;
                    resim.SaveAs(Server.MapPath(tamYolVeri));
                    string veritabaninaEklenecekUrl = "/images/kullaniciresimleri/" + DosyaAdi + uzanti;// başında ~ yok
                    reklamlar.resimurl = veritabaninaEklenecekUrl;
                }      
                db.Entry(reklamlar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(reklamlar);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
