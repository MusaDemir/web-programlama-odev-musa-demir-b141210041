﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    [_KullaniciAdminmiKontrol(Order = 3)]
    [_KullaniciSessionControl(Order = 2)]
    [_KullaniciCookieKontrol(Order = 1)]
    public class adminmarkaislemleriController : Controller
    {
        private eticaretEntities db = new eticaretEntities();

        // GET: /adminmarkaislemleri/
        public ActionResult Index()
        {
            return View(db.markalar.ToList());
        }

        // GET: /adminmarkaislemleri/Details/5
        public ActionResult Details(string dil,int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            markalar markalar = db.markalar.Find(id);
            if (markalar == null)
            {
                return HttpNotFound();
            }
            return View(markalar);
        }

        // GET: /adminmarkaislemleri/Create
        public ActionResult Create(string dil)
        {
            return View();
        }

        // POST: /adminmarkaislemleri/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="markaID,markaAd")] markalar markalar,string dil)
        {
            if (ModelState.IsValid)
            {
                db.markalar.Add(markalar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(markalar);
        }

        // GET: /adminmarkaislemleri/Edit/5
        public ActionResult Edit(string dil,int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            markalar markalar = db.markalar.Find(id);
            if (markalar == null)
            {
                return HttpNotFound();
            }
            return View(markalar);
        }

        // POST: /adminmarkaislemleri/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(string dil, [Bind(Include = "markaID,markaAd")] markalar markalar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(markalar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(markalar);
        }

        // GET: /adminmarkaislemleri/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            markalar markalar = db.markalar.Find(id);
            if (markalar == null)
            {
                return HttpNotFound();
            }
            return View(markalar);
        }

        // POST: /adminmarkaislemleri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            markalar markalar = db.markalar.Find(id);
            db.markalar.Remove(markalar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
