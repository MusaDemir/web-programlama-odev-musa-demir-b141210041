﻿using b141210041_MusaDemir.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace b141210041_MusaDemir.Controllers
{
    [_KullaniciCookieKontrol(Order = 1)]
    [_KullaniciSessionControl(Order = 2)]

    public class kullaniciSiparisController : Controller
    {
        private eticaretEntities db = new eticaretEntities();
        [_DilSessionKontrol]
        public ActionResult satinAldiklarim(string dil)
        {
            int id = YardimciMethodlar.KullaniciSessionIDGetir();
            List<ModelkullaniciSiparis> gidecekmodel = new List<ModelkullaniciSiparis>();
            var siparislist = db.siparisler.Where(s => s.aliciID == id).ToList();
            foreach (var sip in siparislist)
            {
                ModelkullaniciSiparis ks = new ModelkullaniciSiparis();
                ks.siparisID = sip.siparisbilgileriID;
                ks.siparisMiktari = (int)sip.siparismiktari;
                ks.kargotakip = sip.kargoTakipNo;

                var ksurun = db.urunler.Where(u => u.urunID == sip.urunID).SingleOrDefault();
                if (ksurun != null)
                {
                    ks.ucret = ksurun.yeniFiyat;
                    ks.baslik = YardimciMethodlar.dilSessionGetir() == "en" ? ksurun.basliken : ksurun.basliktr;
                    ks.resim = ksurun.urunAnaResim;
                    ks.urungurl = ksurun.guvenliurl;
                    ks.urunID = ksurun.urunID;
                }
                ks.aliciOnay = (bool)sip.siparisAliciOnayi;
                ks.saticiOnay = (bool)sip.siparisSaticiOnayi;
                ks.siparisDurum = sip.siparisDurumu;
                gidecekmodel.Add(ks);
            }
            return View(gidecekmodel);
        }
        [_DilSessionKontrol]
        public ActionResult sattiklarim(string dil)
        {
            int id = YardimciMethodlar.KullaniciSessionIDGetir();
            List<ModelkullaniciSiparis> gidecekmodel = new List<ModelkullaniciSiparis>();
            var siparislist = db.siparisler.Where(s => s.saticiID == id).ToList();
            foreach (var sip in siparislist)
            {
                ModelkullaniciSiparis ks = new ModelkullaniciSiparis();
                ks.siparisID = sip.siparisbilgileriID;
                ks.siparisMiktari = (int)sip.siparismiktari;
                ks.kargotakip = sip.kargoTakipNo;

                var ksurun = db.urunler.Where(u => u.urunID == sip.urunID).SingleOrDefault();
                if (ksurun != null)
                {
                    ks.ucret = ksurun.yeniFiyat;
                    ks.baslik = YardimciMethodlar.dilSessionGetir() == "en" ? ksurun.basliken : ksurun.basliktr;
                    ks.resim = ksurun.urunAnaResim;
                    ks.urungurl = ksurun.guvenliurl;
                    ks.urunID = ksurun.urunID;
                    
                }
                ks.aliciOnay =(bool)sip.siparisAliciOnayi;
                ks.saticiOnay = (bool)sip.siparisSaticiOnayi;
                ks.siparisDurum = sip.siparisDurumu;
                gidecekmodel.Add(ks);

            }
            return View(gidecekmodel);
        }
        public ActionResult kargoTakipAyarla(string veri, int id)
        {
            var siparis = db.siparisler.Where(s => s.siparisbilgileriID == id).SingleOrDefault();
            if (siparis != null)
            {
                siparis.kargoTakipNo = veri;
                siparis.siparisDurumu = "Kargoda";
                db.SaveChanges();
                return Content("true");
            }
            return Content("false");
        }
        public ActionResult aliciOnay(int id)
        {
            if (id >= 1)
            {
                var siparis = db.siparisler.Where(s => s.siparisbilgileriID == id).SingleOrDefault();
                if (siparis != null)
                {
                    siparis.siparisAliciOnayi = true;
                    siparis.siparisDurumu = "Alışveriş Tamamlandı";

                    var urun = db.urunler.Where(u => u.urunID == siparis.urunID).SingleOrDefault();
                    urun.stok--;
                    urun.satilan++;
                    db.SaveChanges();
                    return Content("true");
                }
            }
            return Content("false");
        }
        public ActionResult saticiOnay(int id)
        {
            if (id >= 1)
            {
                var siparis = db.siparisler.Where(s => s.siparisbilgileriID == id).SingleOrDefault();
                if (siparis != null)
                {
                    siparis.siparisSaticiOnayi = true;
                    siparis.siparisDurumu = "Alıcı Onay Verdi";
                    db.SaveChanges();
                    return Content("true");
                }
            }
            return Content("false");
        }
    }
}
