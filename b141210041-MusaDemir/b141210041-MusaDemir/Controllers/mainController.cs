﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;
using System.Web.Security;
namespace b141210041_MusaDemir.Controllers
{
   
    [_KullaniciCookieKontrol]
    [_DilSessionKontrol]
    public class mainController : Controller
    {
        //
        // GET: /anasayfa/
        private eticaretEntities db = new eticaretEntities();
        [_MainIndexRouting]
        public ActionResult Index(string dil)
        {
            bool dilEnmi = YardimciMethodlar.dilSessionGetir() == "en" ? true : false;
       
            Modelmain_Index gidecekmodel = new Modelmain_Index();

            List<notuniqueDictionary<string, string>> asayfa6resim = new List<notuniqueDictionary<string, string>>();
            var reklamlar = db.reklamlar.Where(r => r.aciklama.Contains("anasayfa 6 resim")).ToList();
            reklamlar.ForEach(r =>
            {
                asayfa6resim.Add(new notuniqueDictionary<string, string>() { KeyVeri = r.urunguvenliurl, ValueVeri = r.resimurl });
            });
            gidecekmodel.anasayfa6resim = asayfa6resim;
            List<urunler> sliderurunler = new List<urunler>();
            db.anasayfaslider.ToList().ForEach(s => {
                var urun = db.urunler.Where(u => u.urunID == s.urunID).SingleOrDefault();
                sliderurunler.Add(urun);
 
            });
            gidecekmodel.sliderUrunler = sliderurunler;
            gidecekmodel.sonGezilenler = YardimciMethodlar.sonGezilenUrunleriGetir();
            List<notuniqueDictionary<string, string, List<urunMiniStruct>>> tabmenuliste = new List<notuniqueDictionary<string, string, List<urunMiniStruct>>>();
          
            db.anasayfakategoriler.ToList().ForEach(askat=>{

                List<urunMiniStruct> listurunmini = new List<urunMiniStruct>();
                db.urunler.Where(u => u.kategoriID == askat.kategorilerID).OrderByDescending(u=>u.eklenmetarihi).Take(8).ToList().ForEach(u=>{
                    urunMiniStruct urunmini = new urunMiniStruct();
                    urunmini.baslik = dilEnmi ? u.basliken : u.basliktr;
                    urunmini.guvenliUrl = u.guvenliurl;
                    urunmini.resimurl = u.urunAnaResim;
                    urunmini.ucret = " &#x20BA;" + u.yeniFiyat.ToString();
                    urunmini.urunID = u.urunID;
                    listurunmini.Add(urunmini);
                });
                if (dilEnmi)
                {
                    tabmenuliste.Add(new notuniqueDictionary<string, string, List<urunMiniStruct>>() {
                        KeyVeri = askat.kategoriler.guvenliurl,
                        AraDeger = askat.kategoriler.enkategoriAdi,
                        ValueVeri = listurunmini 
                    });
                }
                else
                {
                    tabmenuliste.Add(new notuniqueDictionary<string, string, List<urunMiniStruct>>()
                    {
                        KeyVeri = askat.kategoriler.guvenliurl,
                        AraDeger = askat.kategoriler.kategoriAdi,
                        ValueVeri = listurunmini
                    });
                }
              
        
           });
            gidecekmodel.tabmenuurunler = tabmenuliste;
            return View(gidecekmodel);
        }
        public ActionResult urun(string dil,string urunurl)
        {
            var urun = db.urunler.Where(x => x.guvenliurl == urunurl).SingleOrDefault();
            if (urun != null)
            {
                Modelmain_Urun gidecekveri = new Modelmain_Urun();
                gidecekveri.urunID = urun.urunID;
                gidecekveri.stok = urun.stok;
                gidecekveri.saticiID = urun.saticiID;
                gidecekveri.satici = db.uyeler.Where(x => x.uyeID == urun.saticiID).Select(x => x.kullaniciAdi).SingleOrDefault();
                gidecekveri.urunYildizSayisi = Convert.ToInt32(db.yorumlar.Where(x => x.urunID == urun.urunID).Average(x => x.yildiz));
                gidecekveri.saticiYildizSayisi = Convert.ToInt32(db.yorumlar.Where(x => x.urunsaticiID == urun.saticiID).Average(x => x.yildiz));
                gidecekveri.fiyat = "&#x20BA;" + urun.yeniFiyat.ToString();
                
                List<string> resimler = new List<string>();
                resimler.Add(urun.urunAnaResim);
                db.urunResimler.Where(x => x.urunID == urun.urunID).ToList().ForEach(x => resimler.Add(x.resimUrl));
                gidecekveri.resimUrller = resimler;

                gidecekveri.yorumlar = db.yorumlar.Where(x => x.urunID == urun.urunID).OrderByDescending(x => x.yorumtarihi).ToList();

                var saticinin4urunu = db.urunler.Where(x => x.saticiID == urun.saticiID).Take(4).ToList();
                List<urunMiniStruct> saticiDigerUrunleri = new List<urunMiniStruct>();
                foreach (var item in saticinin4urunu)
                {
                    urunMiniStruct urunst = new urunMiniStruct();
                    if (YardimciMethodlar.dilSessionGetir() == "en")
                    {
                        urunst.baslik = item.basliken;
                    }
                    else
                    {
                        urunst.baslik = item.basliktr;
                    }
                    urunst.resimurl = item.urunAnaResim;
                    urunst.ucret = "&#x20BA;" + item.yeniFiyat.ToString();
                    urunst.urunID = item.urunID;
                    urunst.guvenliUrl = item.guvenliurl;
                    saticiDigerUrunleri.Add(urunst);
                }
                gidecekveri.saticiDigerUrunleri = saticiDigerUrunleri;


                if (YardimciMethodlar.dilSessionGetir() == "en")
                {
                    gidecekveri.baslik = urun.basliken;
                    gidecekveri.urunDetay = urun.detayen;
                    var ozellikler = db.urunOzellikleri.Where(x => x.urunID == urun.urunID).Join(db.kategoriOzellikleri, x => x.kategoriOzellikleriID, y => y.kategoriOzellikleriID, (x, y) => new { detay = x.ozellikDetay, ad = y.ozellikAdien }).ToList();
                    Dictionary<string, string> gidecekozelliklerr = new Dictionary<string, string>();
                    foreach (var item in ozellikler)
                    {
                        gidecekozelliklerr.Add(item.ad, item.detay);
                    }
                    gidecekveri.ozellikler = gidecekozelliklerr;
                }
                else
                {
                    gidecekveri.baslik = urun.basliktr;
                    gidecekveri.urunDetay = urun.detaytr;
                    var ozellikler = db.urunOzellikleri.Where(x => x.urunID == urun.urunID).Join(db.kategoriOzellikleri, x => x.kategoriOzellikleriID, y => y.kategoriOzellikleriID, (x, y) => new { detay = x.ozellikDetay, ad = y.ozellikAditr }).ToList();
                    Dictionary<string, string> gidecekozelliklerr = new Dictionary<string, string>();
                    foreach (var item in ozellikler)
                    {
                        gidecekozelliklerr.Add(item.ad, item.detay);
                    }
                    gidecekveri.ozellikler = gidecekozelliklerr;
                }
                gidecekveri.kategori = YardimciMethodlar.dilSessionGetir() == "en" ? urun.kategoriler.enkategoriAdi : urun.kategoriler.kategoriAdi;
                YardimciMethodlar.sonGezilenlereEkle(urun.urunID);
                return View(gidecekveri);
            }

            return RedirectToAction("index");
        }
        
        [HttpGet]
        public ActionResult kategori(string dil, string g_url, int sayfa)
        {           
            if (g_url != null)
            {
                if (sayfa <= 0)
                {
                    return RedirectToAction("index", "main", new { dil = YardimciMethodlar.dilSessionGetir() });
                }
                int sayfadagosterilecekurunsayisi = 12;
                int atlanacaksayi = (sayfa - 1) * sayfadagosterilecekurunsayisi;
                var kategori = db.kategoriler.Where(x => x.guvenliurl==g_url).SingleOrDefault();
                if (kategori != null)
                {
                    Modelmain_Kategori gidecekmodel = new Modelmain_Kategori();
                    gidecekmodel.cpage = sayfa;
                    List<int> cekilecekurunIdleri = new List<int>();
                    List<int> ara = new List<int>();// belki amelelik yapıyorum ama kafam biraz yandı ilk aklıma gelen çözüm
                    bool ilk = true;
                    int minucret = 0, maxucret = 0;
                   
                    foreach (String key in Request.QueryString.AllKeys)
                    {
                        string odetay = Request.QueryString[key].ToString();
                        if (key!="ucret")
                        {
                            int koid = db.kategoriOzellikleri.Where(o => o.guvenliurl == key).Select(o => o.kategoriOzellikleriID).SingleOrDefault();
                                
                            if (ilk)
                            {
                                db.urunOzellikleri.Where(x => x.ozellikDetay.Contains(odetay) && x.kategoriOzellikleriID == koid).Select(x => x.urunID).ToList().ForEach(x =>
                                {
                                     cekilecekurunIdleri.Add(x);
                                });
                                ilk = false;
                            }
                            else
                            {
                                db.urunOzellikleri.Where(x => x.ozellikDetay.Contains(odetay) && x.kategoriOzellikleriID == koid).Select(x => x.urunID).ToList().ForEach(x =>
                                {
                                    if (cekilecekurunIdleri.Contains(x))
                                    {
                                        ara.Add(x);
                                    }

                                });
                                cekilecekurunIdleri.Clear();
                                ara.ForEach(x => cekilecekurunIdleri.Add(x));
                                ara.Clear();
                            }
                            
                        }
                        else
                        {
                            var ucretlist = Request.QueryString[key].ToString().Split(',');
                            if (ucretlist.Count()==2)
                            {
                                minucret = Convert.ToInt32(ucretlist[0]);
                                maxucret = Convert.ToInt32(ucretlist[1]);
                            }
                            
                        }
                       
                    }
                    cekilecekurunIdleri.Distinct();
                    List<urunler> gidecekurunler = new List<urunler>();
                    if (cekilecekurunIdleri.Count>0)
                    {
                        cekilecekurunIdleri.ForEach(id =>
                        {
                            var item = db.urunler.Where(x => x.kategoriID == kategori.kategoriID && x.urunID == id).SingleOrDefault();
                            if (item != null && !gidecekurunler.Contains(item))
                            {
                                gidecekurunler.Add(item);
                            }


                        });
                    }
                    else
                    {
                        gidecekurunler = db.urunler.Where(x => x.kategoriID == kategori.kategoriID).ToList();
                    }
                    


                    gidecekmodel.maxUcret = db.urunler.Where(x => x.kategoriID == kategori.kategoriID).Max(x => x.yeniFiyat);
                    

                    gidecekmodel.ozellikler = new List<kategori_kOzellikleristruct>();
                    var kategoriozelliklerlist = db.kategoriOzellikleri.Where(x => x.kategoriID == kategori.kategoriID).ToList();
                    kategoriozelliklerlist.ForEach(k =>
                    {
                        List<string> urunozelliklist = new List<string>();
                        var liste= db.urunOzellikleri.Where(u => u.kategoriOzellikleriID == k.kategoriOzellikleriID).Select(u => u.ozellikDetay).ToList();
                        foreach (var item in liste)
                        {
                            if (!urunozelliklist.Contains(item))
                            {
                                urunozelliklist.Add(item);
                            }
                        }

                        if (YardimciMethodlar.dilSessionGetir() == "en")
                        {
                            gidecekmodel.ozellikler.Add(new kategori_kOzellikleristruct { ID = k.kategoriOzellikleriID, ozellikadi = k.ozellikAdien, guvenliurl = k.guvenliurl, ozellikicerik = urunozelliklist });
                        }
                        else
                        {
                            gidecekmodel.ozellikler.Add(new kategori_kOzellikleristruct { ID = k.kategoriOzellikleriID, ozellikadi = k.ozellikAditr, guvenliurl = k.guvenliurl, ozellikicerik = urunozelliklist });
                        }

                    });









                    switch (YardimciMethodlar.kategoriSayfasıUrunSiralamaKriteriGetir())
                    {
                        case YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri.enyeni:
                            {
                                gidecekurunler = gidecekurunler.OrderByDescending(x => x.eklenmetarihi).ToList();
                            }break;
                        case YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri.enpahali:
                            {
                                gidecekurunler = gidecekurunler.OrderByDescending(x => x.yeniFiyat).ToList();
                            }
                            break;
                        case YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri.enucuz:
                            {
                                gidecekurunler = gidecekurunler.OrderBy(x => x.yeniFiyat).ToList();
                            } break;
                        case YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri.coksatan:
                            {
                                gidecekurunler = gidecekurunler.OrderBy(x => x.satilan).ToList();
                            } break;
                        case YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri.puanifazla:
                            {
                                gidecekurunler = gidecekurunler.OrderByDescending(x => YardimciMethodlar.urunYildizGetir(x.urunID)).ToList();
                            } break;
                       
                        default:
                            break;
                    }

                  

                    if (maxucret != 0)
                    {
                        gidecekurunler = gidecekurunler.Where(x => x.yeniFiyat >= minucret && x.yeniFiyat <= maxucret).ToList();
                    }

                    gidecekmodel.urunSayisi = gidecekurunler.Count();

                    gidecekmodel.sayfaSayisi = (gidecekurunler.Count / sayfadagosterilecekurunsayisi);
                    gidecekurunler = gidecekurunler.Skip(atlanacaksayi).Take(sayfadagosterilecekurunsayisi).ToList();
                   
                    if (YardimciMethodlar.dilSessionGetir() == "en")
                    {
                        gidecekmodel.kategoriAd = kategori.enkategoriAdi;
                        gidecekmodel.urunler = new List<urunMiniStruct>();
                        gidecekurunler.ToList().ForEach(x => gidecekmodel.urunler.Add(
                            new urunMiniStruct() { baslik = x.basliken,
                                guvenliUrl = x.guvenliurl,
                                resimurl = x.urunAnaResim,
                                urunID = x.urunID,
                                ucret = " &#x20BA;" + x.yeniFiyat.ToString()
                            }));
                    }
                    else
                    {
                        gidecekmodel.kategoriAd = kategori.kategoriAdi;
                        gidecekmodel.urunler = new List<urunMiniStruct>();
                        gidecekurunler.ToList().ForEach(x => 
                            gidecekmodel.urunler.Add(
                            new urunMiniStruct() {
                                baslik = x.basliktr,
                                guvenliUrl = x.guvenliurl,
                                resimurl = x.urunAnaResim,
                                urunID = x.urunID,
                                ucret = " &#x20BA;" + x.yeniFiyat.ToString() 
                            }));
                       
                    }


              
                    gidecekmodel.minUcret = 0;
                   
                    return View(gidecekmodel); 
                }
              
            }
            return RedirectToAction("index");
        }
        public ActionResult urunsiralamakriteridegistir(string dil,YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri kriter,string redirectURL)
        {
            YardimciMethodlar.kategoriSayfasıUrunSiralamaKriteriAyarla(kriter);
            return Redirect(redirectURL);
        }
       
        public ActionResult sepetim(string dil)
        {
            var liste = YardimciMethodlar.sepetGetir();// key değeri urunid value değeri sayiyi tutuyor
            Dictionary<urunMiniStruct, int> gidecekveri = new Dictionary<urunMiniStruct, int>();
            foreach (var item in liste)
            {
                if (item.Value == 0)// ürün listeden çıkarılmış 0 tane var
                {

                }
                else
                {
                    var urun = db.urunler.Where(u => u.urunID == item.Key).SingleOrDefault();
                    urunMiniStruct gidecekurun = new urunMiniStruct()
                    {
                        urunID = urun.urunID,
                        baslik = YardimciMethodlar.dilSessionGetir() == "tr" ? urun.basliktr : urun.basliken,
                        guvenliUrl = urun.guvenliurl,
                        resimurl = urun.urunAnaResim,
                        ucret = urun.yeniFiyat.ToString()
                    };

                    gidecekveri.Add(gidecekurun, item.Value);
                }
            }
            
            return View(gidecekveri);
        }
        public ActionResult dilDegistir(string dil,string redirectURL)
        {
            YardimciMethodlar.dilSessionAyarla(dil);
           
            if (redirectURL.Contains("/tr"))
            {
                if (dil == "tr") { }
                else
                {
                    redirectURL = YardimciMethodlar.urldiliDegistir(redirectURL, "/tr", "/en");
                }
            }
            else if (redirectURL.Contains("/en"))
            {
                if (dil == "en") { }
                else
                {
                    redirectURL = YardimciMethodlar.urldiliDegistir(redirectURL, "/en", "/tr");
                }
            }
            if (redirectURL == "false")
            {
                return RedirectToAction("index", "main");
            }
            return Redirect(redirectURL);
        }

        [HttpPost]
        public ActionResult yorumYap(string yorum, string urunID, string urunsaticiID, string adSoyad, string email, string yildiz, string redirectURL, string dil = "tr")
        {
            if (ModelState.IsValid)
            {
                yorumlar gelenyorum = new yorumlar();
                gelenyorum.adSoyad = adSoyad;
                gelenyorum.yorum = yorum;
                gelenyorum.urunID = Convert.ToInt32(urunID);
                gelenyorum.urunsaticiID = Convert.ToInt32(urunsaticiID);
                gelenyorum.email = email;
                gelenyorum.yildiz = Convert.ToInt32(yildiz);
                gelenyorum.yorumtarihi = DateTime.Now;
                db.yorumlar.Add(gelenyorum);
                db.SaveChanges();
            }
            return Redirect(redirectURL);
        }
        
        public void sepeteekle(int id, int sayi, string dil)
        {
            YardimciMethodlar.sepeteEkle(id, sayi);
        }
        [HttpGet]
        public ActionResult ara(string dil, string aranacakKelime, int sayfa)
        {
            if (aranacakKelime != null && aranacakKelime.Trim() != "")
            {
                if (sayfa <= 0)
                {
                    return RedirectToAction("index", "main", new { dil = YardimciMethodlar.dilSessionGetir() });
                }
                int sayfadagosterilecekurunsayisi = 12;
                int atlanacaksayi = (sayfa - 1) * sayfadagosterilecekurunsayisi;

                Modelmain_ara gidecekmodel = new Modelmain_ara();
                gidecekmodel.cpage = sayfa;
                var gidecekurunler = db.urunler.Where(u => u.basliktr.Contains(aranacakKelime) || u.basliken.Contains(aranacakKelime)).ToList();

                switch (YardimciMethodlar.kategoriSayfasıUrunSiralamaKriteriGetir())
                {
                    case YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri.enyeni:
                        {
                            gidecekurunler = gidecekurunler.OrderByDescending(x => x.eklenmetarihi).ToList();
                        } break;
                    case YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri.enpahali:
                        {
                            gidecekurunler = gidecekurunler.OrderByDescending(x => x.yeniFiyat).ToList();
                        }
                        break;
                    case YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri.enucuz:
                        {
                            gidecekurunler = gidecekurunler.OrderBy(x => x.yeniFiyat).ToList();
                        } break;
                    case YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri.coksatan:
                        {
                            gidecekurunler = gidecekurunler.OrderBy(x => x.satilan).ToList();
                        } break;
                    case YardimciMethodlar.kategoriSayfasıUrunSiralamaKriterleri.puanifazla:
                        {
                            gidecekurunler = gidecekurunler.OrderByDescending(x => YardimciMethodlar.urunYildizGetir(x.urunID)).ToList();
                        } break;

                    default:
                        break;
                }


                gidecekmodel.urunSayisi = gidecekurunler.Count();

                gidecekmodel.sayfaSayisi = (gidecekurunler.Count / sayfadagosterilecekurunsayisi);

                gidecekurunler = gidecekurunler.Skip(atlanacaksayi).Take(sayfadagosterilecekurunsayisi).ToList();

                if (YardimciMethodlar.dilSessionGetir() == "en")
                {
                    gidecekmodel.urunler = new List<urunMiniStruct>();
                    gidecekurunler.ToList().ForEach(x => gidecekmodel.urunler.Add(
                        new urunMiniStruct()
                        {
                            baslik = x.basliken,
                            guvenliUrl = x.guvenliurl,
                            resimurl = x.urunAnaResim,
                            urunID = x.urunID,
                            ucret = " &#x20BA;" + x.yeniFiyat.ToString()
                        }));
                }
                else
                {
                    gidecekmodel.urunler = new List<urunMiniStruct>();
                    gidecekurunler.ToList().ForEach(x =>
                        gidecekmodel.urunler.Add(
                        new urunMiniStruct()
                        {
                            baslik = x.basliktr,
                            guvenliUrl = x.guvenliurl,
                            resimurl = x.urunAnaResim,
                            urunID = x.urunID,
                            ucret = " &#x20BA;" + x.yeniFiyat.ToString()
                        }));

                }




                return View(gidecekmodel);
            }
            return RedirectToAction("index", "main");
        }
	}
}