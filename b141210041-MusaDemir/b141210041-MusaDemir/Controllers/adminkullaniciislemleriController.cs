﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    [_KullaniciAdminmiKontrol(Order = 3)]
    [_KullaniciSessionControl(Order = 2)]
    [_KullaniciCookieKontrol (Order = 1)]
    public class adminkullaniciislemleriController : Controller
    {
        private eticaretEntities db = new eticaretEntities();

        // GET: /adminkullaniciislemleri/
        [_DilSessionKontrol]
        public ActionResult Index(string dil)
        {
            int sId=YardimciMethodlar.KullaniciSessionIDGetir();
            return View(db.uyeler.Where(x => x.uyeID != sId).ToList());
        }

        // GET: /adminkullaniciislemleri/Details/5
        [_DilSessionKontrol]
        public ActionResult Details(string dil,int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uyeler uyeler = db.uyeler.Find(id);
            if (uyeler == null)
            {
                return HttpNotFound();
            }
            return View(uyeler);
        }
     

        // GET: /adminkullaniciislemleri/Delete/5
        [_DilSessionKontrol]
        public ActionResult Delete(string dil,int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uyeler uyeler = db.uyeler.Find(id);
            if (uyeler == null)
            {
                return HttpNotFound();
            }
            return View(uyeler);
        }

        // POST: /adminkullaniciislemleri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            uyeler uyeler = db.uyeler.Find(id);
            if (uyeler.uyeRol != "admin")
            {
                var adresler = db.adresler.Where(x => x.uyeID == uyeler.uyeID).ToList();
                if (adresler != null)
                {
                    foreach (var item in adresler)
                    {
                        db.adresler.Remove(item);
                        db.SaveChanges();
                    }
                }
                db.uyeler.Remove(uyeler);
                db.SaveChanges();
            }          
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
