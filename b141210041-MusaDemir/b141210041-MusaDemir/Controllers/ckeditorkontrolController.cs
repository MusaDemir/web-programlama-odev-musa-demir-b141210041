﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    public class ckeditorkontrolController : Controller
    {
        //
        // GET: /ckeditorkontrol/
        eticaretEntities db = new eticaretEntities();
        public ActionResult Index()
        {
            return View();
        }
        public void resimyukle(HttpPostedFileWrapper upload)
        {
            if (upload != null)
            {
                string DosyaAdi = Guid.NewGuid().ToString().Replace('-', '1');// sunucumuzda bulunmayan rasgele bir string atıyor aralardaki - leri herhangi bir karakterle değiştiriyoruz
                string uzanti = System.IO.Path.GetExtension(upload.FileName);
                string tamYolVeri = "~/images/kullaniciresimleri/" + DosyaAdi + uzanti;
                upload.SaveAs(Server.MapPath(tamYolVeri));
            }
        }
        public ActionResult uploadPartial()
        {
            var appData = Server.MapPath("~/images/kullaniciresimleri");
            int a=0;
            var images = Directory.GetFiles(appData).Select(x => new imagesviewmodel
            {
                Url = Url.Content("/images/kullaniciresimleri/" + Path.GetFileName(x)),
                ID = a++
            });
            return View(images);
        }
        public ActionResult resimsil(string adres)
        {
            if (adres != null && adres.Trim() != "")
            {

                var resim = db.urunResimler.Where(x => x.resimUrl.Contains(adres)).SingleOrDefault();
                if (resim != null)
                {                               
                    db.urunResimler.Remove(resim);
                    db.SaveChanges();                    
                }
                string fulladres = Request.MapPath("~" + adres);
                if (System.IO.File.Exists(fulladres))
                {
                    System.IO.File.Delete(fulladres);
                    return Content("true");
                }
              
            }
            return Content("false");
        }
	}
}