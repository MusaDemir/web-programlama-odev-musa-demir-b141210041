﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    [_KullaniciAdminmiKontrol(Order = 3)]
    [_KullaniciSessionControl(Order = 2)]
    [_KullaniciCookieKontrol(Order = 1)]
    public class adminAnasayfaSliderIslemleriController : Controller
    {
        private eticaretEntities db = new eticaretEntities();

        // GET: /adminAnasayfaSliderIslemleri/
        public ActionResult Index()
        {
            var sliderlist = db.anasayfaslider.ToList();
            List<ModeladminAnasayfaSlider_listele> veriler = new List<ModeladminAnasayfaSlider_listele>();
            foreach (var item in sliderlist)
            {
                var urun = db.urunler.Where(x => x.urunID == item.urunID).Select(x => new { x.urunAnaResim, x.basliktr }).SingleOrDefault();
                if (urun != null)
                {
                    veriler.Add(new ModeladminAnasayfaSlider_listele() { sliderID = item.ID, baslik = urun.basliktr, resimurl = urun.urunAnaResim });
                }
            }
            return View(veriler);
        }

        // GET: /adminAnasayfaSliderIslemleri/Create
        public ActionResult Create()
        {
            var urunler = db.urunler.Select(x => new SelectListItem { Value = x.urunID.ToString(), Text = x.basliktr });
            ViewBag.urunID = urunler;
            return View();
        }

        // POST: /adminAnasayfaSliderIslemleri/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,urunID")] anasayfaslider anasayfaslider)
        {
            if (ModelState.IsValid)
            {
                db.anasayfaslider.Add(anasayfaslider);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var urunler = db.urunler.Select(x => new SelectListItem { Value = x.urunID.ToString(), Text = x.basliktr });
            ViewBag.urunID = urunler;
            return View(anasayfaslider);
        }
        // GET: /adminAnasayfaSliderIslemleri/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            anasayfaslider anasayfaslider = db.anasayfaslider.Find(id);
            if (anasayfaslider == null)
            {
                return HttpNotFound();
            }
            return View(anasayfaslider);
        }

        // POST: /adminAnasayfaSliderIslemleri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            anasayfaslider anasayfaslider = db.anasayfaslider.Find(id);
            db.anasayfaslider.Remove(anasayfaslider);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
