﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    public class silinecekcopController : Controller
    {
        private eticaretEntities db = new eticaretEntities();

        // GET: /silinecekcop/
        public ActionResult Index()
        {
            return View(db.uyeler.ToList());
        }

        // GET: /silinecekcop/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uyeler uyeler = db.uyeler.Find(id);
            if (uyeler == null)
            {
                return HttpNotFound();
            }
            return View(uyeler);
        }

        // GET: /silinecekcop/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /silinecekcop/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="uyeID,uyeAdSoyad,kullaniciAdi,sifre,telefonNo,ceptelno,profilFoto,bakiye,uyeRol,cinsiyet,dogumTarihi,TCno,eposta,guvenlikSorusu,guvenlikSorusuCevabi")] uyeler uyeler)
        {
            if (ModelState.IsValid)
            {
                db.uyeler.Add(uyeler);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(uyeler);
        }

        // GET: /silinecekcop/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uyeler uyeler = db.uyeler.Find(id);
            if (uyeler == null)
            {
                return HttpNotFound();
            }
            return View(uyeler);
        }

        // POST: /silinecekcop/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="uyeID,uyeAdSoyad,kullaniciAdi,sifre,telefonNo,ceptelno,profilFoto,bakiye,uyeRol,cinsiyet,dogumTarihi,TCno,eposta,guvenlikSorusu,guvenlikSorusuCevabi")] uyeler uyeler)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uyeler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uyeler);
        }

        // GET: /silinecekcop/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            uyeler uyeler = db.uyeler.Find(id);
            if (uyeler == null)
            {
                return HttpNotFound();
            }
            return View(uyeler);
        }

        // POST: /silinecekcop/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            uyeler uyeler = db.uyeler.Find(id);
            db.uyeler.Remove(uyeler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
