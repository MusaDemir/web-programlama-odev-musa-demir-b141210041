﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    [_KullaniciAdminmiKontrol(Order = 3)]
    [_KullaniciSessionControl(Order = 2)]
    [_KullaniciCookieKontrol(Order = 1)]
    public class adminkategoriislemleriController : Controller
    {
        private eticaretEntities db = new eticaretEntities();

        // GET: /adminkategoriislemleri/
        public ActionResult Index()
        {
            return View(db.kategoriler.ToList());
        }

        // GET: /adminkategoriislemleri/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            kategoriler kategoriler = db.kategoriler.Find(id);
            if (kategoriler == null)
            {
                return HttpNotFound();
            }

            var ozellikler = db.kategoriOzellikleri.Where(x => x.kategoriID == id).ToList();

            ViewBag.ozellikler = ozellikler;

            return View(kategoriler);
        }

        // GET: /adminkategoriislemleri/Create
        public ActionResult Create()
        {
            var veriler = db.kategoriler.Where(x => x.anakategoriID == 0).ToList();// ana kategori id sıfır olan kategori üst kategori taşımıyor demektir o yüzden o ana kategoridir onu getir
            List<SelectListItem> liste = new List<SelectListItem>();
            foreach (var item in veriler)
            {
                if (YardimciMethodlar.dilSessionGetir() == "en")
                    liste.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.enkategoriAdi });
                else
                    liste.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.kategoriAdi });
            }
            ViewBag.anakategoriliste = liste;
            return View();
        }

        // POST: /adminkategoriislemleri/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "kategoriID,anakategoriID,enkategoriAdi,kategoriAdi,guvenliurl")] kategoriler kategoriler, IDictionary<string, string> kategoriozellik)
        {
            if (ModelState.IsValid)
            {
                db.kategoriler.Add(kategoriler);
                kategoriler.guvenliurl = YardimciMethodlar.urlTemizle(kategoriler.kategoriAdi);
                db.SaveChanges();
                foreach (var item in kategoriozellik)
                {
                    if (item.Value != "-1" && item.Value != "-1")
                    {
                        kategoriOzellikleri kategoriozellikleriyeni = new kategoriOzellikleri();
                        kategoriozellikleriyeni.kategoriID = kategoriler.kategoriID;
                        kategoriozellikleriyeni.ozellikAdien = item.Value;
                        kategoriozellikleriyeni.ozellikAditr = item.Key;
                        kategoriozellikleriyeni.guvenliurl = YardimciMethodlar.urlTemizle(item.Key);                      
                        bool guvenliaynıkontrol = false;
                        do
                        {
                            guvenliaynıkontrol = false;
                            var aynıurun = db.kategoriOzellikleri.Where(x => x.guvenliurl == kategoriozellikleriyeni.guvenliurl).ToList();
                            if (aynıurun.Count > 1)
                            {
                                Random rnd = new Random();
                                kategoriozellikleriyeni.guvenliurl += rnd.Next(0, int.MaxValue).ToString();
                                guvenliaynıkontrol = true;
                            }
                        } while (guvenliaynıkontrol);

                        db.kategoriOzellikleri.Add(kategoriozellikleriyeni);
                        db.SaveChanges();
                    }                   
                }
                return RedirectToAction("Index");
            }
           
          

            return View(kategoriler);
        }

        // GET: /adminkategoriislemleri/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            kategoriler kategoriler = db.kategoriler.Find(id);
            if (kategoriler == null)
            {
                return HttpNotFound();
            }

       
            var veriler = db.kategoriOzellikleri.Where(x => x.kategoriID == id).ToList();
            List<Modelkullanici_urunkategoriPartial> modelozellikler = new List<Modelkullanici_urunkategoriPartial>();
            foreach (var item in veriler)
            {
                modelozellikler.Add(new Modelkullanici_urunkategoriPartial() { kategoriOzellikleriID = item.kategoriOzellikleriID, ozellikDetay = "", ozellikAdiTR = item.ozellikAditr, ozellikAdiEn = item.ozellikAdien });
            }

            ViewBag.ozellikler = modelozellikler;
        

            return View(kategoriler);
        }

        // POST: /adminkategoriislemleri/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "kategoriID,anakategoriID,enkategoriAdi,kategoriAdi,guvenliurl")] kategoriler kategoriler, IEnumerable<Modelkullanici_urunkategoriPartial> kategoriozellik)
        {
            if (ModelState.IsValid)
            {
                var kategoriozellikleri = db.kategoriOzellikleri.Where(x => x.kategoriID == kategoriler.kategoriID).ToList();

                foreach (var item in kategoriozellikleri)
                {
                    db.kategoriOzellikleri.Remove(item);
                    db.SaveChanges();
                }
                foreach (var item in kategoriozellik)
                {
                    if (item.ozellikAdiTR != "-1" && item.ozellikAdiEn != "-1")// yeni eklenenlerin kategoriozellikIdsine -1 verdik viewda
                    {
                        kategoriOzellikleri kato = new kategoriOzellikleri();
                        kato.ozellikAdien = item.ozellikAdiEn;
                        kato.ozellikAditr = item.ozellikAdiTR;
                        kato.guvenliurl = YardimciMethodlar.urlTemizle(item.ozellikAdiTR);
                        kato.kategoriID = kategoriler.kategoriID;
                        db.kategoriOzellikleri.Add(kato);
                        db.SaveChanges();
                    }
                   
                   
                }
                db.Entry(kategoriler).State = EntityState.Modified;
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }
            return View(kategoriler);
        }

        // GET: /adminkategoriislemleri/Delete/5
        public ActionResult ozelliksil(int id)
        {
            var ozellik = db.kategoriOzellikleri.Find(id);
            if (ozellik!=null)
            {
                db.kategoriOzellikleri.Remove(ozellik);
                db.SaveChanges();
                return Content("true");
            }

            return Content("false");
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            kategoriler kategoriler = db.kategoriler.Find(id);
            if (kategoriler == null)
            {
                return HttpNotFound();
            }
            return View(kategoriler);
        }

        // POST: /adminkategoriislemleri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            kategoriler kategoriler = db.kategoriler.Find(id);
            var ozellikler = db.kategoriOzellikleri.Where(x => x.kategoriID == kategoriler.kategoriID).ToList();
            foreach (var item in ozellikler)
            {
                db.kategoriOzellikleri.Remove(item);
            }

            db.kategoriler.Remove(kategoriler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
       
    }
}
