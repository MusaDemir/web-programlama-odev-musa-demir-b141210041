﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    [_KullaniciCookieKontrol(Order = 1)]
    [_KullaniciSessionControl(Order = 2)]
    [_DilSessionKontrol]
    public class kullaniciAdresislemleriController : Controller
    {
        private eticaretEntities db = new eticaretEntities();

        // GET: /kullaniciAdresislemleri/
        public ActionResult Index(string dil)
        {
            int id = YardimciMethodlar.KullaniciSessionIDGetir();
            var adresler = db.adresler.Where(a => a.uyeID == id).Include(a => a.il).Include(a => a.ilce).Include(a => a.uyeler);
            return View(adresler.ToList());
        }

        // GET: /kullaniciAdresislemleri/Details/5
        public ActionResult Details(int? id, string dil)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            adresler adresler = db.adresler.Find(id);
            if (adresler == null)
            {
                return HttpNotFound();
            }
            return View(adresler);
        }

        // GET: /kullaniciAdresislemleri/Create
        public ActionResult Create(string dil)
        {
            ViewBag.sehirID = new SelectList(db.il, "ID", "ilAdi");
            ViewBag.uyeID = new SelectList(db.uyeler, "uyeID", "uyeAdSoyad");
            return View();
        }

        // POST: /kullaniciAdresislemleri/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "adresID,uyeID,adresTanimi,ad,soyad,firma,sehirID,ilceID,Adres,TCno,cepTel,gecicimi")] adresler adresler, string dil)
        {
            adresler.gecicimi = false;
            if (ModelState.IsValid)
            {
                adresler.il=db.il.Where(i=>i.ID==adresler.sehirID).SingleOrDefault();
                adresler.ilce = db.ilce.Where(i => i.ID == adresler.ilceID).SingleOrDefault();
                db.adresler.Add(adresler);
                db.SaveChanges();
                if (HttpContext.Session["satisadresekle"] != null)
                {
                    HttpContext.Session["satisadresekle"] = null;
                    return RedirectToAction("odeme", "kullanici");
                }
                return RedirectToAction("Index");
            }

            ViewBag.sehirID = new SelectList(db.il, "ID", "ilAdi", adresler.sehirID);
            ViewBag.uyeID = new SelectList(db.uyeler, "uyeID", "uyeAdSoyad", adresler.uyeID);
            return View(adresler);
        }
        public ActionResult ilcegetir(int ilid, string dil)
        {
            if (ilid>0)
            {
                return PartialView("_ilcepartial", db.ilce.Where(ilc => ilc.il.ID == ilid).OrderBy(ilc=>ilc.ilceAdi).ToList()); 
            }
            return Content("Hata");
           
        }

        // GET: /kullaniciAdresislemleri/Edit/5
        public ActionResult Edit(int? id, string dil)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            adresler adresler = db.adresler.Find(id);
            if (adresler == null)
            {
                return HttpNotFound();
            }
            ViewBag.sehirID = new SelectList(db.il, "ID", "ilAdi", adresler.sehirID);
            ViewBag.ilceID = new SelectList(db.ilce, "ID", "ilceAdi", adresler.ilceID);
            ViewBag.uyeID = new SelectList(db.uyeler, "uyeID", "uyeAdSoyad", adresler.uyeID);
            return View(adresler);
        }

        // POST: /kullaniciAdresislemleri/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "adresID,uyeID,adresTanimi,ad,soyad,firma,sehirID,ilceID,Adres,TCno,cepTel,gecicimi")] adresler adresler, string dil)
        {
            if (ModelState.IsValid)
            {
                db.Entry(adresler).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.sehirID = new SelectList(db.il, "ID", "ilAdi", adresler.sehirID);
         
            ViewBag.uyeID = new SelectList(db.uyeler, "uyeID", "uyeAdSoyad", adresler.uyeID);
            return View(adresler);
        }

        // GET: /kullaniciAdresislemleri/Delete/5
        public ActionResult Delete(int? id, string dil)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            adresler adresler = db.adresler.Find(id);
            if (adresler == null)
            {
                return HttpNotFound();
            }
            return View(adresler);
        }

        // POST: /kullaniciAdresislemleri/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id, string dil)
        {
            adresler adresler = db.adresler.Find(id);
            db.adresler.Remove(adresler);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
