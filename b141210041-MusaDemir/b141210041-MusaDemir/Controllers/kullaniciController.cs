﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{

   [_KullaniciCookieKontrol(Order=1)]
   [_KullaniciSessionControl(Order=2)]
 
    public class kullaniciController : Controller
    {
        private eticaretEntities db = new eticaretEntities();
        [_DilSessionKontrol]
        public ActionResult Index(string dil)
        {      
            return View();
            
        }
        [_DilSessionKontrol]
        public ActionResult urunleriListele(string dil)
        {
            int uyeID = YardimciMethodlar.KullaniciSessionIDGetir();
            var urunler = db.urunler.Where(u => u.saticiID == uyeID);
            return View(urunler.ToList());
        }


        [_DilSessionKontrol]
        public ActionResult urunEkle(string dil)
        {
            Modelkullanici_UrunEkle model = new Modelkullanici_UrunEkle();
            var veriler = db.kategoriler.Where(x => x.anakategoriID == 0).ToList();// ana kategori id sıfır olan kategori üst kategori taşımıyor demektir o yüzden o ana kategoridir onu getir
            List<SelectListItem> liste = new List<SelectListItem>();
            foreach (var item in veriler)
            {
                if (YardimciMethodlar.dilSessionGetir() == "en")
                    liste.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.enkategoriAdi });
                else
                    liste.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.kategoriAdi });
            }
            ViewBag.anakategoriliste = liste;
            ViewBag.markaID = new SelectList(db.markalar, "markaID", "markaAd");
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        [_DilSessionKontrol]
        public ActionResult urunEkle(string dil, Modelkullanici_UrunEkle gelenModel)
        {
            if (ModelState.IsValid)
            {
                urunler yeniurun = new urunler();
                yeniurun.kategoriID = gelenModel.kategoriID;
                yeniurun.markaID = gelenModel.markaID;
                yeniurun.saticiID = YardimciMethodlar.KullaniciSessionIDGetir();
                yeniurun.basliktr = gelenModel.basliktr;
                yeniurun.basliken = gelenModel.basliken;
                yeniurun.detaytr = gelenModel.detaytr;
                yeniurun.detayen = gelenModel.detayen;
                yeniurun.eklenmetarihi = DateTime.Now;


                yeniurun.yeniFiyat = gelenModel.yeniFiyat;
                yeniurun.stok = gelenModel.stok;
                yeniurun.guvenliurl = YardimciMethodlar.urlTemizle(gelenModel.basliktr);
               
                
                bool guvenliaynıkontrol = false;
                do
                {
                    guvenliaynıkontrol = false;
                    var aynıurun = db.urunler.Where(x => x.guvenliurl == yeniurun.guvenliurl).SingleOrDefault();
                    if (aynıurun != null)
                    {
                        Random rnd = new Random();
                        yeniurun.guvenliurl += rnd.Next(0, int.MaxValue).ToString();
                        guvenliaynıkontrol = true;
                    }
                } while (guvenliaynıkontrol);

                yeniurun.eskiFiyat = 0;
                yeniurun.satilan = 0;
                db.urunler.Add(yeniurun);
                db.SaveChanges();

                bool ilkmi = true;// ilk resim ürünün ana resmi olacak bu resim anasayfa vs yerlerde ilk çekilecek resim olacak
                foreach (var resim in gelenModel.resimler)
                {
                    if (resim.ContentLength > 0)
                    {
                        string DosyaAdi = Guid.NewGuid().ToString().Replace('-', '1');// sunucumuzda bulunmayan rasgele bir string atıyor aralardaki - leri herhangi bir karakterle değiştiriyoruz
                        string uzanti = System.IO.Path.GetExtension(resim.FileName);
                        string tamYolVeri = "~/images/kullaniciresimleri/" + DosyaAdi + uzanti;
                        resim.SaveAs(Server.MapPath(tamYolVeri));
                        string veritabaninaEklenecekUrl = "/images/kullaniciresimleri/" + DosyaAdi + uzanti;// başında ~ yok
                        if (ilkmi)
                        {
                            yeniurun.urunAnaResim = veritabaninaEklenecekUrl;
                            ilkmi = false;
                        }
                        else
                        {// ilk resimden sonraki resimler alt resimler olarak urun resimlere eklensin
                            urunResimler urunresimler = new urunResimler();
                            urunresimler.urunID = yeniurun.urunID;
                            urunresimler.resimUrl = veritabaninaEklenecekUrl;
                            db.urunResimler.Add(urunresimler);
                            db.SaveChanges();
                        }


                    }
                }
                db.SaveChanges();




                foreach (var item in gelenModel.kategoriozellik)// ram kg vs vs
                {// özellikler dictionary olarak geliyor key değeri kategoriozelliklerinin ıd si, value si ise ozellik detayini tutuyor     
                    if (item.Key != "-1" && item.Value != "-1")
                    {
                        urunOzellikleri urunozellik = new urunOzellikleri();
                        urunozellik.urunID = yeniurun.urunID;
                        urunozellik.kategoriOzellikleriID = Convert.ToInt32(item.Key);
                        urunozellik.ozellikDetay = item.Value;
                        db.urunOzellikleri.Add(urunozellik);
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("urunleriListele", new { dil = YardimciMethodlar.dilSessionGetir() });
            }
            var veriler = db.kategoriler.Where(x => x.anakategoriID == 0).ToList();// ana kategori id sıfır olan kategori üst kategori taşımıyor demektir o yüzden o ana kategoridir onu getir
            List<SelectListItem> liste = new List<SelectListItem>();
            foreach (var item in veriler)
            {
                if (YardimciMethodlar.dilSessionGetir() == "en")
                    liste.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.enkategoriAdi });
                else
                    liste.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.kategoriAdi });
            }
            ViewBag.anakategoriliste = liste;
            ViewBag.markaID = new SelectList(db.markalar, "markaID", "markaAd");
            return View(gelenModel);
        }

        [HttpGet]
        [_DilSessionKontrol]
        public ActionResult urunDuzenle(string dil, int urunID)
        {
            var urundbveri = db.urunler.Where(x => x.urunID == urunID).ToList().Take(1);
            urunler gelenurun = null;
            foreach (var item in urundbveri)
            {
                gelenurun = item;
            }

            if (gelenurun != null && gelenurun.saticiID == YardimciMethodlar.KullaniciSessionIDGetir())
            {
                var araIdlist = db.kategoriler.Where(x => x.kategoriID == gelenurun.kategoriID).Select(x => x.anakategoriID).Take(1).ToList();
                int araid = 0;
                foreach (var item in araIdlist)
                {
                    araid = (int)item;
                }
                var anaidlist = db.kategoriler.Where(x => x.kategoriID == araid).Select(x => x.anakategoriID).Take(1).ToList();
                int anaid = 0;
                foreach (var item in anaidlist)
                {
                    anaid = (int)item;
                }

                // ana kategori id sıfır olan kategori üst kategori taşımıyor demektir o yüzden o ana kategoridir onu getir
                var anamenulist = db.kategoriler.Where(x => x.anakategoriID == 0).ToList();
                List<SelectListItem> listeanamenu = new List<SelectListItem>();
                foreach (var item in anamenulist)
                {
                    if (YardimciMethodlar.dilSessionGetir() == "en")
                        listeanamenu.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.enkategoriAdi, Selected = item.kategoriID == anaid });
                    else
                        listeanamenu.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.kategoriAdi, Selected = item.kategoriID == anaid });
                }

                List<SelectListItem> listealtmenu = new List<SelectListItem>();
                var altmenulist = db.kategoriler.Where(x => x.anakategoriID == anaid).ToList();
                foreach (var item in altmenulist)
                {
                    if (YardimciMethodlar.dilSessionGetir() == "en")
                        listealtmenu.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.enkategoriAdi, Selected = item.kategoriID == araid });
                    else
                        listealtmenu.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.kategoriAdi, Selected = item.kategoriID == araid });
                }

                List<SelectListItem> listeurunmenu = new List<SelectListItem>();
                var urunmenulist = db.kategoriler.Where(x => x.anakategoriID == araid).ToList();
                foreach (var item in urunmenulist)
                {
                    if (YardimciMethodlar.dilSessionGetir() == "en")
                        listeurunmenu.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.enkategoriAdi, Selected = item.kategoriID == gelenurun.kategoriID });
                    else
                        listeurunmenu.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.kategoriAdi, Selected = item.kategoriID == gelenurun.kategoriID });
                }

                ViewBag.anakategoriliste = listeanamenu;
                ViewBag.arakategoriliste = listealtmenu;
                ViewBag.urunkategoriliste = listeurunmenu;

                List<string> resimyollari = db.urunResimler.Where(x => x.urunID == gelenurun.urunID).Select(x => x.resimUrl).ToList();
                ViewBag.urunresimleri = resimyollari;

                ViewBag.markaID = new SelectList(db.markalar, "markaID", "markaAd");
                return View(gelenurun);
            }
            return RedirectToAction("urunleriListele", new { dil = YardimciMethodlar.dilSessionGetir() });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult urunDuzenle([Bind(Include = "urunID,kategoriID,markaID,saticiID,basliktr,basliken,detaytr,detayen,yeniFiyat,stok")]urunler gelenurun, IEnumerable<HttpPostedFileBase> resimler, IDictionary<string, string> kategoriozellik)
        {
            if (ModelState.IsValid)
            {
                urunler urun = db.urunler.Where(x => x.urunID == gelenurun.urunID).SingleOrDefault();
                urun.basliktr = gelenurun.basliktr;
                urun.basliken = gelenurun.basliken;
                urun.detaytr = gelenurun.detaytr;
                urun.detayen = gelenurun.detayen;
                urun.kategoriID = gelenurun.kategoriID;
                urun.markaID = gelenurun.markaID;
                urun.yeniFiyat = gelenurun.yeniFiyat;
                urun.stok = gelenurun.stok;
                bool ilkmi = true;

                foreach (var resim in resimler)
                {
                    if (resim != null && resim.ContentLength > 0)
                    {
                        string DosyaAdi = Guid.NewGuid().ToString().Replace('-', '1');// sunucumuzda bulunmayan rasgele bir string atıyor aralardaki - leri herhangi bir karakterle değiştiriyoruz
                        string uzanti = System.IO.Path.GetExtension(resim.FileName);
                        string tamYolVeri = "~/images/kullaniciresimleri/" + DosyaAdi + uzanti;
                        resim.SaveAs(Server.MapPath(tamYolVeri));
                        string veritabaninaEklenecekUrl = "/images/kullaniciresimleri/" + DosyaAdi + uzanti;// başında ~ yok
                        if (ilkmi)
                        {
                            string fullPath = Request.MapPath("~" + urun.urunAnaResim);

                            urunResimler urunresimler = new urunResimler();// tek resim ekleniyorsa onu anaresim yap ana resmi diğerlerine ekle
                            urunresimler.urunID = urun.urunID;
                            urunresimler.resimUrl = urun.urunAnaResim;
                            db.urunResimler.Add(urunresimler);
                            db.SaveChanges();

                            urun.urunAnaResim = veritabaninaEklenecekUrl;
                            ilkmi = false;
                        }
                        else
                        {// ilk resimden sonraki resimler alt resimler olarak urun resimlere eklensin
                            urunResimler urunresimler = new urunResimler();
                            urunresimler.urunID = urun.urunID;
                            urunresimler.resimUrl = veritabaninaEklenecekUrl;
                            db.urunResimler.Add(urunresimler);
                            db.SaveChanges();
                        }


                    }
                }

                var eskiozellikler = db.urunOzellikleri.Where(x => x.urunID == urun.urunID).ToList();
                foreach (var item in eskiozellikler)
                {
                    db.urunOzellikleri.Remove(item);
                    db.SaveChanges();
                }

                foreach (var item in kategoriozellik)// ram kg vs vs
                {// özellikler dictionary olarak geliyor key değeri kategoriozelliklerinin ıd si, value si ise ozellik detayini tutuyor     
                    if (item.Key != "-1" && item.Value != "-1")
                    {
                        urunOzellikleri urunozellik = new urunOzellikleri();
                        urunozellik.urunID = urun.urunID;
                        urunozellik.kategoriOzellikleriID = Convert.ToInt32(item.Key);
                        urunozellik.ozellikDetay = item.Value;
                        db.urunOzellikleri.Add(urunozellik);
                        db.SaveChanges();
                    }
                }





                db.SaveChanges();
            }
            return RedirectToAction("urunleriListele", new { dil = YardimciMethodlar.dilSessionGetir() });
        }

        [HttpGet]
        public ActionResult urunSil(int urunID)
        {
            urunler urunler = db.urunler.Find(urunID);

            if (urunler.saticiID == YardimciMethodlar.KullaniciSessionIDGetir())// kullanicinin olmayan ürünleri silme
            {
                string fullPath = Request.MapPath("~" + urunler.urunAnaResim);
                if (System.IO.File.Exists(fullPath))
                {
                    System.IO.File.Delete(fullPath);
                }


                var urunresimler = db.urunResimler.Where(x => x.urunID == urunler.urunID).ToList();//Ürünün diğer resimlerini sil
                foreach (var item in urunresimler)
                {
                    fullPath = Request.MapPath("~" + item.resimUrl);
                    if (System.IO.File.Exists(fullPath))
                    {
                        System.IO.File.Delete(fullPath);
                    }
                    db.urunResimler.Remove(item);
                    db.SaveChanges();
                }
                db.urunOzellikleri.Where(x => x.urunID == urunler.urunID).ToList().ForEach(x => db.urunOzellikleri.Remove(x));



                db.urunler.Remove(urunler);
                db.SaveChanges();
            }
            return RedirectToAction("urunleriListele");
        }

        public ActionResult odeme()
        {
            HttpContext.Session["satisadresekle"] = true;// eğer kullanıcı create sayfasına giderse ödeme sayfasına direkt dönmesi için kod
            int id = YardimciMethodlar.KullaniciSessionIDGetir();
            var adresler = db.adresler.Where(a => a.uyeID == id).ToList();
            ViewBag.adresID = new SelectList(adresler,"adresID","adresTanimi",null);
            return View();
        }
        [HttpPost]
        public ActionResult odeme(Modelkullanici_odeme odememodel)
        {
            var sepetlist = YardimciMethodlar.sepetGetir();
            int aliciID = YardimciMethodlar.KullaniciSessionIDGetir();
            bool siparisoldu = false;
            foreach (var item in sepetlist)
            {
                int urunid = item.Key;
                int miktar = item.Value;

                siparisler yenisiparis = new siparisler();
                yenisiparis.aliciID = aliciID;
                yenisiparis.adreslerID = odememodel.adresID;
                yenisiparis.aliciNotu = odememodel.aciklama == null ? "" : odememodel.aciklama;
                yenisiparis.kargoTakipNo = "Belirtilmedi";
                yenisiparis.siparisAliciOnayi = false;
                yenisiparis.siparisSaticiOnayi = false;
                yenisiparis.siparismiktari = miktar;                
                yenisiparis.siparisDurumu = "Sipariş Saticiya Bildirildi";
                yenisiparis.siparisTarihi = DateTime.Now;
                yenisiparis.urunID = urunid;

                int saticiID = db.urunler.Where(u => u.urunID == urunid).Select(u => u.saticiID).SingleOrDefault();
                yenisiparis.saticiID = saticiID;
                db.siparisler.Add(yenisiparis);
                db.SaveChanges();
              
            }
            if (siparisoldu)
            {
                YardimciMethodlar.sepetiTemizle();
                return RedirectToAction("satinAldiklarim", "kullaniciSiparis", new { dil = YardimciMethodlar.dilSessionGetir() }); 
            }            
            return RedirectToAction("index","main");
        }

        public ActionResult sifredegistir()
        {
            return View();
        }
        [HttpPost]
        public ActionResult sifredegistir(string eskisifre, string yenisifre, string yenisifretekrar)
        {
            if (yenisifre == yenisifretekrar)
            {
                int id = YardimciMethodlar.KullaniciSessionIDGetir();
                var uye = db.uyeler.Where(u => u.uyeID == id).SingleOrDefault();
                if (uye != null)
                {
                    eskisifre = YardimciMethodlar.StringSifrele(eskisifre);
                    if (uye.sifre==eskisifre)
                    {
                        uye.sifre = YardimciMethodlar.StringSifrele(yenisifre);
                        db.SaveChanges();
                        YardimciMethodlar.KullaniciSessionIDSil();
                        return RedirectToAction("index", "main");
                    }
                
                }
            }


            return RedirectToAction("index");
        }
        public ActionResult emaildegistir()
        {
            return View();
        }
        [HttpPost]
        public ActionResult emaildegistir(string eskiemail, string yeniemail, string yeniemailtekrar)
        {
            if (yeniemail == yeniemailtekrar)
            {
                int id = YardimciMethodlar.KullaniciSessionIDGetir();
                var uye = db.uyeler.Where(u => u.uyeID == id).SingleOrDefault();


                if (uye != null && uye.eposta == eskiemail)
                {
                    uye.eposta = yeniemail;
                    db.SaveChanges();
                    YardimciMethodlar.KullaniciSessionIDSil();
                    return RedirectToAction("index", "main");
                }

            }


            return RedirectToAction("index");
        }

        #region KategoriPartialViewlari
        [HttpGet]
        public ActionResult resimSil(string adres)
        {// ürün düzenle sayfasında ajax ile resmi silmeye yarayan method
            var resim = db.urunResimler.Where(x => x.resimUrl.Contains(adres)).SingleOrDefault();
            if (resim != null)
            {
                int saticiID = db.urunler.Where(x => x.urunID == resim.urunID).Select(x => x.saticiID).Single();
                if (saticiID == YardimciMethodlar.KullaniciSessionIDGetir())
                {
                    string fulladres = Request.MapPath("~" + resim.resimUrl);
                    if (System.IO.File.Exists(fulladres))
                    {
                        System.IO.File.Delete(fulladres);
                    }
                    db.urunResimler.Remove(resim);
                    db.SaveChanges();
                    return Content("true");
                }
            }
            return Content("false");
        }
        [HttpGet]
        public ActionResult kategoriozellik(int id)
        {// her kategorinin farklı özellikleri var ram kg vs gibi bunları çekip partiala gönder partial onlar için bir input list oluştursun
        
            var veriler = db.kategoriOzellikleri.Where(x => x.kategoriID == id).ToList();
            List<Modelkullanici_urunkategoriPartial> model = new List<Modelkullanici_urunkategoriPartial>();
            foreach (var item in veriler)
            {
                model.Add(new Modelkullanici_urunkategoriPartial() { kategoriOzellikleriID = item.kategoriOzellikleriID, ozellikDetay = "", ozellikAdiTR = item.ozellikAditr, ozellikAdiEn = item.ozellikAdien });
            }

            return PartialView("_uruneklekategoriOzellikleri", model);
        }
        [HttpGet]
        public ActionResult kategoriozellikDoluGetir(int katid,int urunid)
        {// ürünün kategorileri özelliklerini getir ve doldur
            var veriler = db.kategoriOzellikleri.Where(x => x.kategoriID == katid).ToList();
            List<Modelkullanici_urunkategoriPartial> model = new List<Modelkullanici_urunkategoriPartial>();
            foreach (var item in veriler)
            {
                string urundetay = db.urunOzellikleri.Where(x => x.kategoriOzellikleriID == item.kategoriOzellikleriID && x.urunID == urunid).Select(x => x.ozellikDetay).SingleOrDefault();
                model.Add(new Modelkullanici_urunkategoriPartial() { kategoriOzellikleriID = item.kategoriOzellikleriID, ozellikDetay = urundetay, ozellikAdiTR = item.ozellikAditr, ozellikAdiEn = item.ozellikAdien });
            }

            return PartialView("_uruneklekategoriOzellikleri", model);
        }


        [HttpGet]
        public ActionResult altkategorileriGetir(int id)
        { // 3 kategori türü var biri en üst biri orta biri en alt yani elektronik -> pc -> laptop
            //gibi ilk kategori controllerdan gönderiliyor. pcyani ikinci kısım bu partial ile viewda tıklanan ana kategorinin alt kategorileri olarak gönderiliyor
            List<SelectListItem> liste = new List<SelectListItem>();
            //return PartialView("_yeniilanPartial", model);
            var veriler = db.kategoriler.Where(x => x.anakategoriID == id).ToList();
            foreach (var item in veriler)
            {
                if (YardimciMethodlar.dilSessionGetir() == "en")
                    liste.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.enkategoriAdi });
                else
                    liste.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.kategoriAdi });
            }
            return PartialView("_altkategoripartial", liste);
        }
        [HttpGet]
        public ActionResult urunkategorileriGetir(int id)
        {// bu ise alt kategorinin de altı ürünün kategorilerini getiriyor
            List<SelectListItem> liste = new List<SelectListItem>();
            //return PartialView("_yeniilanPartial", model);
            var veriler = db.kategoriler.Where(x => x.anakategoriID == id).ToList();
            foreach (var item in veriler)
            {
                if (YardimciMethodlar.dilSessionGetir() == "en")
                    liste.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.enkategoriAdi });
                else
                    liste.Add(new SelectListItem { Value = item.kategoriID.ToString(), Text = item.kategoriAdi });

            }
            return PartialView("_urunkategoriPartial", liste);
        } 
        #endregion
	}

   
}