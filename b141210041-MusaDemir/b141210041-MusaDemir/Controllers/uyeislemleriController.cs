﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using b141210041_MusaDemir.Models;

namespace b141210041_MusaDemir.Controllers
{
    [_KullaniciCookieKontrol]    
    public class uyeislemleriController : Controller
    {
        private eticaretEntities db = new eticaretEntities();
        // GET: /uyeislemleri/
        public ActionResult Index()
        {
            return RedirectToAction("index", "main");
        }
        public ActionResult giris()
        {
            if (YardimciMethodlar.KullaniciSessionIDGetir()==0)
            {
                return View();
            }
            return RedirectToAction("index", "main", new { dil = YardimciMethodlar.dilSessionGetir() });
        }      
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult giris(ModelgirisYap model, string returnurl,string returnUrl)
        {
            if (ModelState.IsValid)
            {
                string sifre = YardimciMethodlar.StringSifrele(model.Sifre);
                var uye = db.uyeler.Where(x => x.eposta == model.Email && x.sifre == sifre).SingleOrDefault();
                if (uye != null)
                {
                    YardimciMethodlar.KullaniciSessionIDAyarla(uye.uyeID);
                    if (model.BeniHatirla)
                    {
                        YardimciMethodlar.CookieEkle("cID", uye.uyeID.ToString());
                    }
                    if (returnUrl == null)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        return Redirect(returnUrl);
                    }

                }
                else
                {
                    ModelState.AddModelError("", "EMail veya şifre hatalı!");
                }

                
            }
            return View();
        }

        public ActionResult cikis()
        {
            YardimciMethodlar.KullaniciSessionIDSil();
            YardimciMethodlar.CookieSil("cID");
            return RedirectToAction("Index");
        }

        //public ActionResult kayitol()
        //{
        //    return RedirectToAction("index", "main");
        //}
        //[AllowAnonymous]
        //[HttpPost]
        //[ValidateInput(false)]
        //[ValidateAntiForgeryToken]
        //public ActionResult kayitol([Bind(Include = "kullaniciAdi,eposta,sifre,cinsiyet,dogumTarihi,TCno,guvenlikSorusu,guvenlikSorusuCevabi")]uyeler model, string returnurl)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        Random rnd = new Random();
        //        model.sifre = YardimciMethodlar.StringSifrele(model.sifre);
        //        model.uyeRol = "kullanici";
        //        model.bakiye = 0;
        //        db.uyeler.Add(model);
        //        db.SaveChanges();
        //    }
           
        //    return RedirectToAction("index","main");
        //}

        public ActionResult kayitol()
        {
            return RedirectToAction("index", "main");
        }

        // POST: /silinecekcop/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult kayitol([Bind(Include = "uyeID,uyeAdSoyad,kullaniciAdi,sifre,telefonNo,ceptelno,profilFoto,bakiye,cinsiyet,dogumTarihi,TCno,eposta,guvenlikSorusu,guvenlikSorusuCevabi")] uyeler uyeler, HttpPostedFileBase resim)
        {
            if (ModelState.IsValid)
            {
                uyeler.uyeRol = "uye";
                uyeler.bakiye = 0;

                if (resim != null)
                {
                    string DosyaAdi = Guid.NewGuid().ToString().Replace('-', '1');// sunucumuzda bulunmayan rasgele bir string atıyor aralardaki - leri herhangi bir karakterle değiştiriyoruz
                    string uzanti = System.IO.Path.GetExtension(resim.FileName);
                    string tamYolVeri = "~/images/kullaniciresimleri/" + DosyaAdi + uzanti;
                    resim.SaveAs(Server.MapPath(tamYolVeri));
                    string veritabaninaEklenecekUrl = "/images/kullaniciresimleri/" + DosyaAdi + uzanti;// başında ~ yok
                    uyeler.profilFoto = veritabaninaEklenecekUrl;
                }
                else
                {
                    uyeler.profilFoto = "/images/home/logo.png";
                }



                uyeler.ceptelno = "";
                uyeler.telefonNo = "";
                uyeler.kullaniciAdi = uyeler.eposta;
                uyeler.sifre = YardimciMethodlar.StringSifrele(uyeler.sifre);
                db.uyeler.Add(uyeler);
                db.SaveChanges();

                YardimciMethodlar.CookieEkle("cID", uyeler.uyeID.ToString());
                YardimciMethodlar.KullaniciSessionIDAyarla(uyeler.uyeID);
                return RedirectToAction("Index");
            }

            return View(uyeler);
        }


        
	}
}