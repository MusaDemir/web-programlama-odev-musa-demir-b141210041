﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace b141210041_MusaDemir
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            /*------------------- Main Controller---------------------*/
            routes.MapRoute(
              "anasayfalarUrunGoster",
              "{dil}/urun/{urunurl}",
              new { controller = "main", action = "urun"}
          );

            routes.MapRoute(
             "anasayfalarKategoriler",
             "{dil}/kategori/{sayfa}/{g_url}",
             new { controller = "main", action = "kategori" }
         );
            routes.MapRoute(
              "anasayfaara",
              "{dil}/{sayfa}/ara",
              new { controller = "main", action = "ara" }
          );
            routes.MapRoute(
             "anasayfalarSepetim",
             "{dil}/sepetim",
             new { controller = "main", action = "sepetim" }
         );
            routes.MapRoute(
              "anasayfalarIndex",
              "{dil}/anasayfa",
              new { controller = "main", action = "Index" }
          );
            routes.MapRoute(
              "maindildegistir",
              "{dil}/dildegistir/{returnURL}",
              new { controller = "main", action = "dilDegistir" }
          );


           

            /*------------------- Kullanici Controller---------------------*/

            routes.MapRoute(
              "kullaniciindex",
              "{dil}/kullanici/profil",
              new { controller = "kullanici", action = "Index" }
          );
            routes.MapRoute(
              "kullaniciodeme",
              "{dil}/satinal",
              new { controller = "kullanici", action = "odeme" }
          );
            routes.MapRoute(
              "kullaniciurunekle",
              "{dil}/kullanici/urunekle",
              new { controller = "kullanici", action = "urunEkle" }
          );
            routes.MapRoute(
              "kullaniciurunduzenle",
              "{dil}/kullanici/urunduzenle/{urunID}",
              new { controller = "kullanici", action = "urunDuzenle" }
          );
            routes.MapRoute(
            "kullaniciurunlistele",
            "{dil}/kullanici/urunlerim",
            new { controller = "kullanici", action = "urunleriListele" }
        );

            /*------------------- Uyeislemleri Controller---------------------*/

            routes.MapRoute(
             "uyeislemleriGirisYap",
             "{dil}/girisyap/{returnUrl}",
             new { controller = "uyeislemleri", action = "giris" ,returnUrl=UrlParameter.Optional}
         );

            /*------------------- adminkullaniciislemleri Controller---------------------*/

            routes.MapRoute(
           "kullanicilarilistele",
           "{dil}/admin/tumkullanicilar",
           new { controller = "adminkullaniciislemleri", action = "Index" }
       );
            routes.MapRoute(
           "kullanicidetayi",
           "{dil}/admin/kullanicidetayi/{id}",
           new { controller = "adminkullaniciislemleri", action = "Details" }
       );
            routes.MapRoute(
           "kullanicisil",
           "{dil}/admin/kullanicisil/{id}",
           new { controller = "adminkullaniciislemleri", action = "Delete" }
       );
            /*------------------- adminanasyafakategoriislemleri Controller---------------------*/

            routes.MapRoute(
           "adminanasayfakateegorilistele",
           "{dil}/admin/anasayfakategorileri",
           new { controller = "adminanasayfakategoriislemleri", action = "Index" }
       );
            routes.MapRoute(
           "adminanasayfakateegoriolustur",
           "{dil}/admin/anasayfakategoriolustur",
           new { controller = "adminanasayfakategoriislemleri", action = "Create" }
       );
            routes.MapRoute(
           "adminanasayfakateegorisil",
           "{dil}/admin/anasayfakategorisil/{id}",
           new { controller = "adminanasayfakategoriislemleri", action = "Delete" }
       );
            /*------------------- kullanicsiparisiislemleri Controller---------------------*/

            routes.MapRoute(
           "kullanicisattiklarim",
           "{dil}/kullanici/sattiklarim",
           new { controller = "kullaniciSiparis", action = "sattiklarim" }
       );
            routes.MapRoute(
           "kullanicisiparislerim",
           "{dil}/kullanici/siparislerim",
           new { controller = "kullaniciSiparis", action = "satinAldiklarim" }
       );
          


            /*------------------- adminyorumislemleri Controller---------------------*/

            routes.MapRoute(
            "yorumlarilistele",
            "{dil}/admin/tumyorumlar",
            new { controller = "adminyorumislemleri", action = "Index" }
                );
            routes.MapRoute(
            "yorumdetayi",
            "{dil}/admin/yorumdetayi/{id}",
            new { controller = "adminyorumislemleri", action = "Details" }
                );
            routes.MapRoute(
            "yorumsil",
            "{dil}/admin/yorumsil/{id}",
            new { controller = "adminyorumislemleri", action = "Delete" }
                );
            routes.MapRoute(
                "yorumduzenle",
                "{dil}/admin/yorumduzenle/{id}",
                new { controller = "adminyorumislemleri", action = "Edit" }
                );
            /*------------------- adminkategoriislemleri Controller---------------------*/

            routes.MapRoute(
           "kategorilerilistele",
           "{dil}/admin/tumkategoriler",
           new { controller = "adminkategoriislemleri", action = "Index" }
               );
            routes.MapRoute(
          "kategoriekle",
          "{dil}/admin/kategoriekle",
          new { controller = "adminkategoriislemleri", action = "Create" }
              );
            routes.MapRoute(
            "kategoridetayi",
            "{dil}/admin/kategoridetayi/{id}",
            new { controller = "adminkategoriislemleri", action = "Details" }
                );
            routes.MapRoute(
            "kategorisil",
            "{dil}/admin/kategorisil/{id}",
            new { controller = "adminkategoriislemleri", action = "Delete" }
                );
            routes.MapRoute(
                "kategoriduzenle",
                "{dil}/admin/kategoriduzenle/{id}",
                new { controller = "adminkategoriislemleri", action = "Edit" }
                );


            /*------------------- adminreklam Controller---------------------*/

            routes.MapRoute(
           "reklamlarilistele",
           "{dil}/admin/tumreklamlar",
           new { controller = "adminreklam", action = "Index" }
               );  
          
            routes.MapRoute(
                "reklamduzenle",
                "{dil}/admin/reklamduzenle/{id}",
                new { controller = "adminreklam", action = "Edit" }
                );


            /*------------------- adminAnasayfaSliderislemleri Controller---------------------*/

            routes.MapRoute(
            "asayfasliderlistele",
            "{dil}/admin/sliderurunleri",
            new { controller = "adminAnasayfaSliderIslemleri", action = "Index" }
                );
            routes.MapRoute(
              "asayfasliderekle",
              "{dil}/admin/sliderurunekle",
              new { controller = "adminAnasayfaSliderIslemleri", action = "Create" }
              );
          
            routes.MapRoute(
            "asayfaslidersil",
            "{dil}/admin/sliderurunsil/{id}",
            new { controller = "adminAnasayfaSliderIslemleri", action = "Delete" }
                );

            /*------------------- adminkategoriislemleri Controller---------------------*/

            routes.MapRoute(
           "markaliste",
           "{dil}/admin/markaliste",
           new { controller = "adminmarkaislemleri", action = "Index" }
               );
            routes.MapRoute(
          "markaekle",
          "{dil}/admin/markaekle",
          new { controller = "adminmarkaislemleri", action = "Create" }
              );          
            routes.MapRoute(
            "markasil",
            "{dil}/admin/markasil/{id}",
            new { controller = "adminmarkaislemleri", action = "Delete" }
                );
            routes.MapRoute(
                "markaduzenle",
                "{dil}/admin/markaduzenle/{id}",
                new { controller = "adminmarkaislemleri", action = "Edit" }
                );

            /*------------------- kullaniciAdresislemleri Controller---------------------*/
            routes.MapRoute(
              "kullaniciadreslerim",
              "{dil}/kullanici/adreslerim",
              new { controller = "kullaniciAdresislemleri", action = "index" }
              );
                routes.MapRoute(
                 "kullaniciadresdetay",
                 "{dil}/kullanici/adresdetay/{id}",
                 new { controller = "kullaniciAdresislemleri", action = "Details" }
             );
                routes.MapRoute(
                 "kullaniciadresolustur",
                 "{dil}/kullanici/adresolustur",
                 new { controller = "kullaniciAdresislemleri", action = "Create" }
             );
                routes.MapRoute(
                  "kullaniciadresduzenle",
                  "{dil}/kullanici/adresduzenle/{id}",
                  new { controller = "kullaniciAdresislemleri", action = "Edit" }
              );
                routes.MapRoute(
                  "kullaniciadressil",
                  "{dil}/kullanici/adressil/{id}",
                  new { controller = "kullaniciAdresislemleri", action = "Delete" }
              );




            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{gurl}",
                defaults: new { controller = "main", action = "Index", gurl = UrlParameter.Optional }
            );
        }
    }
}
