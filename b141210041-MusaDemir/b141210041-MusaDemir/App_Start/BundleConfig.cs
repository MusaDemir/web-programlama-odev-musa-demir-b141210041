﻿using System.Web;
using System.Web.Optimization;

namespace b141210041_MusaDemir
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            /*
                <script src="js/jquery.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.scrollUp.min.js"></script>
  <script src="js/price-range.js"></script>
  <script src="js/jquery.prettyPhoto.js"></script>
  <script src="js/main.js"></script>*/
            bundles.Add(new ScriptBundle("~/bundles/mainjs").Include(
               "~/Scripts/jquery.js",
                "~/Scripts/sweetalert-dev.js",
                 "~/Scripts/bootstrap.min.js",
                  "~/Scripts/jquery.scrollUp.min.js",
                   "~/Scripts/price-range.js",
                    "~/Scripts/jquery.prettyPhoto.js",
                     "~/Scripts/main.js",
                     "~/ckeditor/ckeditor.js"
                      
                ));

            /*
             * <script src="~/ckeditor/ckeditor.js"></script>
              <link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/font-awesome.min.css" rel="stylesheet">
<link href="css/prettyPhoto.css" rel="stylesheet">
<link href="css/price-range.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/main.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">*/
            bundles.Add(new StyleBundle("~/Content/mainstyles").Include(
                      "~/Content/bootstrap.min.css",
                       "~/Content/sweetalert.css",
                       "~/Content/font-awesome.min.css",
                        "~/Content/prettyPhoto.css",
                         "~/Content/price-range.css",
                          "~/Content/animate.css",
                           "~/Content/main.css",
                            "~/Content/responsive.css"
                            ));
        }
    }
}