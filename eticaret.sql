USE [master]
GO
/****** Object:  Database [eticaret]    Script Date: 13.12.2016 12:29:28 ******/
CREATE DATABASE [eticaret]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'eticaret', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.MSSQLSERVER2\MSSQL\DATA\eticaret.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'eticaret_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.MSSQLSERVER2\MSSQL\DATA\eticaret_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [eticaret] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [eticaret].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [eticaret] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [eticaret] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [eticaret] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [eticaret] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [eticaret] SET ARITHABORT OFF 
GO
ALTER DATABASE [eticaret] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [eticaret] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [eticaret] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [eticaret] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [eticaret] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [eticaret] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [eticaret] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [eticaret] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [eticaret] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [eticaret] SET  DISABLE_BROKER 
GO
ALTER DATABASE [eticaret] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [eticaret] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [eticaret] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [eticaret] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [eticaret] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [eticaret] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [eticaret] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [eticaret] SET RECOVERY FULL 
GO
ALTER DATABASE [eticaret] SET  MULTI_USER 
GO
ALTER DATABASE [eticaret] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [eticaret] SET DB_CHAINING OFF 
GO
ALTER DATABASE [eticaret] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [eticaret] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [eticaret] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'eticaret', N'ON'
GO
USE [eticaret]
GO
/****** Object:  Table [dbo].[adresler]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[adresler](
	[adresID] [int] IDENTITY(1,1) NOT NULL,
	[uyeID] [int] NOT NULL,
	[adresTanimi] [text] NULL,
	[ad] [nvarchar](250) NOT NULL,
	[soyad] [nvarchar](250) NOT NULL,
	[firma] [nvarchar](250) NULL,
	[sehirID] [int] NOT NULL,
	[ilceID] [int] NOT NULL,
	[Adres] [text] NOT NULL,
	[TCno] [text] NOT NULL,
	[cepTel] [text] NULL,
	[gecicimi] [bit] NULL,
 CONSTRAINT [PK_adresler] PRIMARY KEY CLUSTERED 
(
	[adresID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[anasayfaslider]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[anasayfaslider](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NULL,
 CONSTRAINT [PK_anasayfaslider] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[fiyatAlarm]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[fiyatAlarm](
	[fiyatAlarmID] [int] IDENTITY(1,1) NOT NULL,
	[uyeID] [int] NOT NULL,
	[urunID] [int] NOT NULL,
	[mevcutfiyat] [float] NOT NULL,
 CONSTRAINT [PK_fiyatAlarm] PRIMARY KEY CLUSTERED 
(
	[fiyatAlarmID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[il]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[il](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ilKodu] [int] NULL,
	[ilAdi] [nvarchar](255) NULL,
 CONSTRAINT [PK_il] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ilce]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ilce](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ilKodu] [int] NULL,
	[ilceKodu] [int] NULL,
	[ilceAdi] [nvarchar](255) NULL,
 CONSTRAINT [PK_ilce] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[kategoriler]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[kategoriler](
	[kategoriID] [int] IDENTITY(1,1) NOT NULL,
	[anakategoriID] [int] NULL CONSTRAINT [DF_kategoriler_anakategoriID]  DEFAULT ((0)),
	[enkategoriAdi] [nvarchar](100) NOT NULL,
	[kategoriAdi] [nvarchar](100) NOT NULL,
	[guvenliurl] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_kategoriler] PRIMARY KEY CLUSTERED 
(
	[kategoriID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[kategoriOzellikleri]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[kategoriOzellikleri](
	[kategoriOzellikleriID] [int] IDENTITY(1,1) NOT NULL,
	[kategoriID] [int] NOT NULL,
	[ozellikAdien] [nvarchar](50) NOT NULL,
	[ozellikAditr] [nvarchar](50) NOT NULL,
	[guvenliurl] [nvarchar](100) NULL,
 CONSTRAINT [PK_kategoriOzellikleri] PRIMARY KEY CLUSTERED 
(
	[kategoriOzellikleriID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[markalar]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[markalar](
	[markaID] [int] IDENTITY(1,1) NOT NULL,
	[markaAd] [text] NOT NULL,
 CONSTRAINT [PK_markalar] PRIMARY KEY CLUSTERED 
(
	[markaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[reklamlar]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reklamlar](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[resimurl] [text] NULL,
	[urunguvenliurl] [text] NULL,
	[aciklama] [text] NULL,
 CONSTRAINT [PK_anasayfa6minireklam] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[sepet]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sepet](
	[sepetID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[uyeID] [int] NOT NULL,
 CONSTRAINT [PK_sepet] PRIMARY KEY CLUSTERED 
(
	[sepetID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[siparisler]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[siparisler](
	[siparisbilgileriID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[saticiID] [int] NOT NULL,
	[aliciID] [int] NOT NULL,
	[siparisSaticiOnayi] [bit] NULL,
	[siparisAliciOnayi] [bit] NULL,
	[siparisDurumu] [nvarchar](500) NULL,
	[siparisTarihi] [datetime] NOT NULL,
	[siparisKontrol] [bit] NULL,
	[siparisTutari] [float] NOT NULL,
	[aliciNotu] [text] NULL,
	[kargoTakipNo] [text] NULL,
 CONSTRAINT [PK_siparisler] PRIMARY KEY CLUSTERED 
(
	[siparisbilgileriID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[urunKampanyalar]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[urunKampanyalar](
	[urunKampanyaID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[indirimOrani] [int] NULL,
	[baslangic] [datetime] NOT NULL,
	[bitis] [datetime] NOT NULL,
	[kampanyaBasigi] [text] NOT NULL,
	[kampanyaAciklamasi] [text] NULL,
	[kampantaGorseli] [text] NULL,
 CONSTRAINT [PK_urunKampanyalar] PRIMARY KEY CLUSTERED 
(
	[urunKampanyaID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[urunler]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[urunler](
	[urunID] [int] IDENTITY(1,1) NOT NULL,
	[kategoriID] [int] NOT NULL,
	[markaID] [int] NOT NULL,
	[saticiID] [int] NOT NULL,
	[basliktr] [text] NOT NULL,
	[basliken] [text] NULL,
	[detaytr] [text] NULL,
	[detayen] [text] NOT NULL,
	[urunAnaResim] [text] NULL,
	[yeniFiyat] [float] NOT NULL,
	[eskiFiyat] [float] NULL,
	[stok] [int] NOT NULL,
	[satilan] [int] NULL,
	[eklenmetarihi] [datetime] NOT NULL,
	[guvenliurl] [nvarchar](250) NULL,
 CONSTRAINT [PK_urunler] PRIMARY KEY CLUSTERED 
(
	[urunID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[urunOzellikleri]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[urunOzellikleri](
	[urunOzelliklerID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[kategoriOzellikleriID] [int] NOT NULL,
	[ozellikDetay] [text] NULL,
 CONSTRAINT [PK_urunOzellikleri] PRIMARY KEY CLUSTERED 
(
	[urunOzelliklerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[urunResimler]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[urunResimler](
	[urunResimlerID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[resimUrl] [text] NOT NULL,
 CONSTRAINT [PK_urunResimler] PRIMARY KEY CLUSTERED 
(
	[urunResimlerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[uyeler]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[uyeler](
	[uyeID] [int] IDENTITY(1,1) NOT NULL,
	[uyeAdSoyad] [text] NOT NULL,
	[kullaniciAdi] [nvarchar](250) NOT NULL,
	[sifre] [nvarchar](1000) NOT NULL,
	[telefonNo] [text] NULL,
	[ceptelno] [text] NULL,
	[profilFoto] [text] NULL,
	[bakiye] [float] NOT NULL CONSTRAINT [DF_uyeler_bakiye]  DEFAULT ((0)),
	[uyeRol] [nvarchar](50) NOT NULL,
	[cinsiyet] [nchar](10) NOT NULL,
	[dogumTarihi] [date] NOT NULL,
	[TCno] [nvarchar](12) NOT NULL,
	[eposta] [nvarchar](500) NOT NULL,
	[guvenlikSorusu] [text] NOT NULL,
	[guvenlikSorusuCevabi] [nvarchar](500) NOT NULL,
 CONSTRAINT [PK_uyeler] PRIMARY KEY CLUSTERED 
(
	[uyeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[yeniUyeOnay]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[yeniUyeOnay](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[adSoyad] [text] NULL,
	[email] [nvarchar](500) NULL,
	[sifre] [nvarchar](250) NULL,
	[onayKodu] [nvarchar](500) NULL,
 CONSTRAINT [PK_onaybekleyen] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[yorumlar]    Script Date: 13.12.2016 12:29:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[yorumlar](
	[yorumID] [int] IDENTITY(1,1) NOT NULL,
	[urunID] [int] NOT NULL,
	[urunsaticiID] [int] NULL,
	[adSoyad] [nvarchar](500) NOT NULL,
	[email] [nvarchar](500) NOT NULL,
	[yorum] [text] NOT NULL,
	[yildiz] [int] NULL,
	[yorumtarihi] [date] NULL,
 CONSTRAINT [PK_yorumlar] PRIMARY KEY CLUSTERED 
(
	[yorumID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[adresler] ADD  CONSTRAINT [DF_adresler_gecicimi]  DEFAULT ((1)) FOR [gecicimi]
GO
ALTER TABLE [dbo].[siparisler] ADD  CONSTRAINT [DF_siparisler_siparisSaticiOnayi]  DEFAULT ((0)) FOR [siparisSaticiOnayi]
GO
ALTER TABLE [dbo].[siparisler] ADD  CONSTRAINT [DF_siparisler_siparisAliciOnayi]  DEFAULT ((0)) FOR [siparisAliciOnayi]
GO
ALTER TABLE [dbo].[adresler]  WITH CHECK ADD  CONSTRAINT [FK_adresler_il1] FOREIGN KEY([sehirID])
REFERENCES [dbo].[il] ([ID])
GO
ALTER TABLE [dbo].[adresler] CHECK CONSTRAINT [FK_adresler_il1]
GO
ALTER TABLE [dbo].[adresler]  WITH CHECK ADD  CONSTRAINT [FK_adresler_ilce1] FOREIGN KEY([ilceID])
REFERENCES [dbo].[ilce] ([ID])
GO
ALTER TABLE [dbo].[adresler] CHECK CONSTRAINT [FK_adresler_ilce1]
GO
ALTER TABLE [dbo].[adresler]  WITH CHECK ADD  CONSTRAINT [FK_adresler_uyeler] FOREIGN KEY([uyeID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[adresler] CHECK CONSTRAINT [FK_adresler_uyeler]
GO
ALTER TABLE [dbo].[fiyatAlarm]  WITH CHECK ADD  CONSTRAINT [FK_fiyatAlarm_urunler1] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[fiyatAlarm] CHECK CONSTRAINT [FK_fiyatAlarm_urunler1]
GO
ALTER TABLE [dbo].[fiyatAlarm]  WITH CHECK ADD  CONSTRAINT [FK_fiyatAlarm_uyeler1] FOREIGN KEY([uyeID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[fiyatAlarm] CHECK CONSTRAINT [FK_fiyatAlarm_uyeler1]
GO
ALTER TABLE [dbo].[ilce]  WITH CHECK ADD  CONSTRAINT [FK_ilce_il] FOREIGN KEY([ilKodu])
REFERENCES [dbo].[il] ([ID])
GO
ALTER TABLE [dbo].[ilce] CHECK CONSTRAINT [FK_ilce_il]
GO
ALTER TABLE [dbo].[kategoriOzellikleri]  WITH CHECK ADD  CONSTRAINT [FK_kategoriOzellikleri_kategoriler1] FOREIGN KEY([kategoriID])
REFERENCES [dbo].[kategoriler] ([kategoriID])
GO
ALTER TABLE [dbo].[kategoriOzellikleri] CHECK CONSTRAINT [FK_kategoriOzellikleri_kategoriler1]
GO
ALTER TABLE [dbo].[sepet]  WITH CHECK ADD  CONSTRAINT [FK_sepet_urunler1] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[sepet] CHECK CONSTRAINT [FK_sepet_urunler1]
GO
ALTER TABLE [dbo].[sepet]  WITH CHECK ADD  CONSTRAINT [FK_sepet_uyeler1] FOREIGN KEY([uyeID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[sepet] CHECK CONSTRAINT [FK_sepet_uyeler1]
GO
ALTER TABLE [dbo].[siparisler]  WITH CHECK ADD  CONSTRAINT [FK_siparisler_urunler] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[siparisler] CHECK CONSTRAINT [FK_siparisler_urunler]
GO
ALTER TABLE [dbo].[siparisler]  WITH CHECK ADD  CONSTRAINT [FK_siparisler_uyeler] FOREIGN KEY([saticiID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[siparisler] CHECK CONSTRAINT [FK_siparisler_uyeler]
GO
ALTER TABLE [dbo].[siparisler]  WITH CHECK ADD  CONSTRAINT [FK_siparisler_uyeler1] FOREIGN KEY([aliciID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[siparisler] CHECK CONSTRAINT [FK_siparisler_uyeler1]
GO
ALTER TABLE [dbo].[urunKampanyalar]  WITH CHECK ADD  CONSTRAINT [FK_urunKampanyalar_urunler1] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[urunKampanyalar] CHECK CONSTRAINT [FK_urunKampanyalar_urunler1]
GO
ALTER TABLE [dbo].[urunler]  WITH CHECK ADD  CONSTRAINT [FK_urunler_kategoriler] FOREIGN KEY([kategoriID])
REFERENCES [dbo].[kategoriler] ([kategoriID])
GO
ALTER TABLE [dbo].[urunler] CHECK CONSTRAINT [FK_urunler_kategoriler]
GO
ALTER TABLE [dbo].[urunler]  WITH CHECK ADD  CONSTRAINT [FK_urunler_markalar] FOREIGN KEY([markaID])
REFERENCES [dbo].[markalar] ([markaID])
GO
ALTER TABLE [dbo].[urunler] CHECK CONSTRAINT [FK_urunler_markalar]
GO
ALTER TABLE [dbo].[urunler]  WITH CHECK ADD  CONSTRAINT [FK_urunler_uyeler] FOREIGN KEY([saticiID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[urunler] CHECK CONSTRAINT [FK_urunler_uyeler]
GO
ALTER TABLE [dbo].[urunOzellikleri]  WITH CHECK ADD  CONSTRAINT [FK_urunOzellikleri_urunler1] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[urunOzellikleri] CHECK CONSTRAINT [FK_urunOzellikleri_urunler1]
GO
ALTER TABLE [dbo].[urunResimler]  WITH CHECK ADD  CONSTRAINT [FK_urunResimler_urunler1] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[urunResimler] CHECK CONSTRAINT [FK_urunResimler_urunler1]
GO
ALTER TABLE [dbo].[yorumlar]  WITH CHECK ADD  CONSTRAINT [FK_yorumlar_urunler] FOREIGN KEY([urunID])
REFERENCES [dbo].[urunler] ([urunID])
GO
ALTER TABLE [dbo].[yorumlar] CHECK CONSTRAINT [FK_yorumlar_urunler]
GO
ALTER TABLE [dbo].[yorumlar]  WITH CHECK ADD  CONSTRAINT [FK_yorumlar_uyeler] FOREIGN KEY([urunsaticiID])
REFERENCES [dbo].[uyeler] ([uyeID])
GO
ALTER TABLE [dbo].[yorumlar] CHECK CONSTRAINT [FK_yorumlar_uyeler]
GO
USE [master]
GO
ALTER DATABASE [eticaret] SET  READ_WRITE 
GO
